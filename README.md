This repository contains code to generate important points and key configurations

This software is licensed under [GPL2+](LICENSE.GPL2), which is required by [ALGLIB](https://www.alglib.net/download.php).

For additional licensing options, please contract [The Office of Technology Commercialization, The University of Texas at Austin](https://research.utexas.edu/otc/).

# Build Instructions

## Step 1: Setup vcpkg

Follow (the official instructions)[https://github.com/Microsoft/vcpkg] to install vcpkg on your system.

The recommended location is `C:/src/vcpkg`. A longer path may block some packages' build.

## Step 2: Install dependencies

The following packages are required:
* cgal
* eigen3
* glfw3

vcpkg will install additional dependencies for you. Note: boost is also a dependency. Hence it might take over 30 minutes to install.

## Step 3: Install cmake

Get cmake at https://cmake.org/download/ and install it.
During the installation you may want to put it to `$PATH` to simplify later steps.

## Step 4: create VCPKG_ROOT environment variable

Our CMakeLists.txt can figure out how to use packages installed by vcpkg automatically once VCPKG_ROOT is set.
This environment variable is supposed to be the directory which stores `vcpkg.exe`.

## Step 5: Build

It's recommended to create a `build/` directory under the root of the source code, and run `cmake ..` within it.

The visual studio solution file and corresponding project files will be created there.
