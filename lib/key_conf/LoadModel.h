/**
 * SPDX-FileCopyrightText: Copyright © 2020 McGill University, The University of Texas at Austin
 * SPDX-FileContributor: Modified by Xinya Zhang <xinyazhang@utexas.edu>
 * SPDX-FileContributor: Robert Belfer <belfer2470@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
#ifndef LOAD_MODEL_H
#define LOAD_MODEL_H

#include <string>
#include <vector>
#include <Eigen/Core>

#include "PQP/PQP.h"

namespace geokeyconf {

void loadPQPModel(PQP_Model* model, Eigen::MatrixXd V, Eigen::MatrixXi F);

void loadAll(PQP_Model* pqpEnv, PQP_Model* pqpRobot, const std::string& fEnv, const std::string& fRob);

template <typename DerivedV, typename DerivedF>
bool read_triangle_mesh(
	const std::string str,
	Eigen::PlainObjectBase<DerivedV>& V,
	Eigen::PlainObjectBase<DerivedF>& F);

template <typename Scalar, typename Index>
bool readOBJ(
	FILE * obj_file,
	std::vector<std::vector<Scalar > > & V,
	std::vector<std::vector<Scalar > > & TC,
	std::vector<std::vector<Scalar > > & N,
	std::vector<std::vector<Index > > & F,
	std::vector<std::vector<Index > > & FTC,
	std::vector<std::vector<Index > > & FN);

}

#endif
