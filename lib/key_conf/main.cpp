/**
 * SPDX-FileCopyrightText: Copyright © 2020 McGill University, The University of Texas at Austin
 * SPDX-FileContributor: Modified by Xinya Zhang <xinyazhang@utexas.edu>
 * SPDX-FileContributor: Robert Belfer <belfer2470@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
#include <iostream>

#include <math.h>
#include <Eigen/Core>

#include <fstream>
#include "PQP.h"

#include "KeySampler.h"
#include "LoadModel.h"
#include "files.h"

PQP_Model* pqpEnv   = new PQP_Model;
PQP_Model* pqpRobot = new PQP_Model;

std::string puzzleName;

std::vector<Eigen::VectorXd>
getImportantPoints(std::string fName)
{
	std::vector<Eigen::VectorXd> result;
	std::string line;
	std::ifstream file(fName);
	Eigen::VectorXd pairs1(7);
	while (std::getline(file, line)) {
		int i = 0;
		double value;
		std::istringstream iss(line);
		while (iss >> value) {
			pairs1(i) = value;
			i++;
		}
		result.push_back(pairs1);
	}
	return result;
}

std::vector<Eigen::VectorXd>
getKeyConfigs(std::string fName)
{
	std::vector<Eigen::VectorXd> result;
	std::string line;
	std::ifstream file(fName);
	Eigen::VectorXd keyConfig(9);
	while (std::getline(file, line)) {
		int i = 0;
		double value;
		std::istringstream iss(line);
		while (iss >> value) {
			keyConfig(i) = value;
			i++;
		}
		result.push_back(keyConfig);
	}
	return result;
}

#if 0
 int main(int argc, char* argv[])
 {
	 std::string Inp = argc > 1 ? argv[1] : "doublemPuzzle";



	 std::clock_t start = std::clock();

	 std::vector<Eigen::VectorXd> Imp1;
	 std::vector<Eigen::VectorXd> Imp2;
	 std::string envf;
	 std::string robf;


	 enum Puzzle { alphaPuzzle, elkPuzzle, threadPuzzle, doublemPuzzle, testPuzzle, enigma };
	 Puzzle puzzle = testPuzzle;
	 double search = 360;

	 if (Inp == "doublemPuzzle") puzzle = doublemPuzzle;
	 if (Inp == "alphaPuzzle") puzzle = alphaPuzzle;
	 if (Inp == "elkPuzzle") puzzle = elkPuzzle;
	 if (Inp == "threadPuzzle") puzzle = threadPuzzle;
	 if (Inp == "testPuzzle") puzzle = testPuzzle;
	 if (Inp == "enigma") puzzle = enigma;


	 switch (puzzle)
	 {
	 case alphaPuzzle:
		 envf = "C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\alphaPuzzle\\ompl_env.obj";	//world obj file
		 robf = "C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\alphaPuzzle\\ompl_robot.obj";	//robot obj file
		 loadAll(pqpEnv, pqpRobot, envf, robf);		//store these in a pqp model
		 Imp1 = getImportantPoints("C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\alphaPuzzle\\ompl_env_geodesicPoints.txt");	//world important ponits
		 Imp2 = getImportantPoints("C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\alphaPuzzle\\ompl_robot_geodesicPoints.txt");	//robot important points
		 puzzleName = "alphaPuzzle";
		 break;
	 case elkPuzzle:
		 envf = "C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\elkPuzzle\\elkEnv.obj";
		 robf = "C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\elkPuzzle\\elkRobot.obj";
		 loadAll(pqpEnv, pqpRobot, envf, robf);
		 Imp1 = getImportantPoints("C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\elkPuzzle\\elkEnvPoints.txt");
		 Imp2 = getImportantPoints("C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\elkPuzzle\\elkRobotPoints.txt");
		 puzzleName = "elkPuzzle";
		 break;
	 case threadPuzzle:
		 envf = "C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\threadPuzzle\\ThreadedBox.obj";
		 robf = "C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\threadPuzzle\\ThreadedRod.obj";
		 loadAll(pqpEnv, pqpRobot, envf, robf);
		 Imp1 = getImportantPoints("C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\threadPuzzle\\ThreadedTunnelPoints.txt");
		 Imp2 = getImportantPoints("C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\threadPuzzle\\ThreadedRodPoints.txt");
		 puzzleName = "threadPuzzle";
		 break;
	 case doublemPuzzle:
		 envf = "C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\doublemPuzzle\\doublemPuzzleEnv95.obj";
		 robf = "C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\doublemPuzzle\\doublemPuzzleRob95.obj";
		 loadAll(pqpEnv, pqpRobot, envf, robf);
		 Imp1 = getImportantPoints("C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\doublemPuzzle\\doublemPuzzleEnvPoints.txt");
		 Imp2 = getImportantPoints("C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\doublemPuzzle\\doublemPuzzleRobPoints.txt");
		 puzzleName = "doublemPuzzle";
		 break;
	 case testPuzzle:
		 envf = "C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\testPuzzle\\cube1.obj";
		 robf = "C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\testPuzzle\\cube2.obj";
		 loadAll(pqpEnv, pqpRobot, envf, robf);
		 Imp1 = getImportantPoints("C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\testPuzzle\\i1.txt");
		 Imp2 = getImportantPoints("C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\testPuzzle\\i2.txt");
		 puzzleName = "testPuzzle";
	 case enigma:
		 envf = "C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\enigmaPuzzle\\enigma1.obj";
		 robf = "C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\enigmaPuzzle\\enigma2.obj";
		 loadAll(pqpEnv, pqpRobot, envf, robf);
		 Imp1 = getImportantPoints("C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\enigmaPuzzle\\enigma1Points.txt");
		 Imp2 = getImportantPoints("C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\models\\enigmaPuzzle\\enigma2Points.txt");
		 puzzleName = "enigmaPuzzle";
	 default:
		 break;
	 }
     

	 std::cout << "Models loaded." << std::endl;

	 std::ofstream myFile;
	 std::string finp = "C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\Logs\\" + puzzleName + "\\Valid Region\\Key Configurations.txt";	//location of key configurations
	 std::vector<Eigen::VectorXd> KeyStates;
	 if (exists(finp)) {
		 KeyStates = getKeyConfigs(finp);
	 }
	 else {
		 myFile.open(finp);
		 for (int i = 0; i < Imp1.size(); i++) {
			 for (int j = 0; j < Imp2.size(); j++) {
				 std::cout << "Calculating for pair " << i << " " << j << std::endl;
				 std::vector<Eigen::VectorXd> SomeKeyStates = KeyConfigs(puzzleName, i, j, Imp1.at(i), Imp2.at(j), search);
				 Eigen::VectorXd* it = SomeKeyStates.data();
				 for (int k = 0; k < SomeKeyStates.size(); k++) {
					 (*(it + k))(7) = i;
					 (*(it + k))(8) = j;
				 }
				 KeyStates.insert(KeyStates.end(), SomeKeyStates.begin(), SomeKeyStates.end());
			 }
		 }

		 std::clock_t key = std::clock();


		 for (auto KeyConfig : KeyStates) {
			 for (int i = 0; i < KeyConfig.size(); i++) {
				 myFile << KeyConfig(i) << " ";
			 }
			 myFile << '\n';

		 }
		 myFile.close();
	 }
 
     return 0;
 }
#endif
