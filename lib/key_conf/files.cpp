/**
 * SPDX-FileCopyrightText: Copyright © 2020 McGill University, The University of Texas at Austin
 * SPDX-FileContributor: Modified by Xinya Zhang <xinyazhang@utexas.edu>
 * SPDX-FileContributor: Robert Belfer <belfer2470@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
#include <fstream>
#include <iostream>
#include <filesystem>

#include "files.h"

bool exists(const std::string& name) {
	std::ifstream f(name);
	if (f) return true;
	return false;
}

std::string getAvailable(std::string name) {
	int i = 1;
	while (exists(name + std::to_string(i) + ".txt")) {
		i++;
	}
	return name + std::to_string(i) + ".txt";
}
