/**
 * SPDX-FileCopyrightText: Copyright © 2020 McGill University, The University of Texas at Austin
 * SPDX-FileContributor: Modified by Xinya Zhang <xinyazhang@utexas.edu>
 * SPDX-FileContributor: Robert Belfer <belfer2470@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
#include <fstream>
#include <map>
#include <queue>
#include <random>
#include <iostream>
#include <math.h>

#include "PQP/PQP.h"
#include "KeySampler.h"
#include "LoadModel.h"

namespace {

constexpr int STATE_DIMENSION = 7;

Eigen::Matrix3d
vectorRotate(Eigen::Vector3d current, Eigen::Vector3d target)
{
	current.normalize();
	target.normalize();
	Eigen::Matrix3d G;
	G << current.dot(target), -current.cross(target).norm(), 0, current.cross(target).norm(), current.dot(target),
	        0, 0, 0, 1;
	Eigen::Matrix3d Finv;
	Eigen::Vector3d u, v, w;
	u = current;
	v = (target - current.dot(target) * current) / (target - current.dot(target) * current).norm();
	w = target.cross(current);
	Finv << u(0), v(0), w(0), u(1), v(1), w(1), u(2), v(2), w(2);
	return Finv * G * Finv.inverse();
}

Eigen::Matrix3d
orthoRotate(Eigen::Vector3d robot, Eigen::Vector3d env)
{
	Eigen::Vector3d target;
	if (env(0) != 0 || env(2) != 0)
		target = {-env(2), 0, env(0)};
	if (env(0) != 0 || env(1) != 0)
		target = {-env(1), env(0), 0};
	return vectorRotate(robot, target);
}

#if 0
double
round(double original, int precision)
{
	double temp = original * pow(10, precision);
	temp        = round(temp);
	temp        = temp / pow(10, precision);
	return temp;
}
#endif

auto
assembly(const std::vector<Eigen::VectorXd> in)
{
	Eigen::MatrixXd ret;
	if (in.empty())
		return ret;
	ret.resize(in.size(), in.front().rows());
	for (size_t i = 0; i < in.size(); i++)
		ret.row(i) = in[i];
	return ret;
}

auto
assembly(const std::vector<Eigen::Vector2i> in)
{
	Eigen::MatrixXi ret;
	if (in.empty())
		return ret;
	ret.resize(in.size(), in.front().rows());
	for (size_t i = 0; i < in.size(); i++)
		ret.row(i) = in[i];
	return ret;
}

auto
assembly(const std::vector<int> in)
{
	Eigen::VectorXi ret;
	if (in.empty())
		return ret;
	ret.resize(in.size(), 1);
	for (size_t i = 0; i < in.size(); i++)
		ret(i) = in[i];
	return ret;
}

// Could be templates but trying not to be over-engineering
auto
cat(const std::vector<Eigen::VectorXi> in)
{
	Eigen::VectorXi ret;
	if (in.empty())
		return ret;
	size_t rows = 0;
	for (const auto& e : in)
		rows += e.rows();
	ret.resize(rows, 1);
	size_t row_at = 0;
	for (size_t i = 0; i < in.size(); i++) {
		const auto& e = in[i];
		ret.block(row_at, 0, e.rows(), e.cols()) = e;
		row_at += e.rows();
	}
	return ret;
}

auto
cat(const std::vector<Eigen::MatrixXd> in)
{
	Eigen::MatrixXd ret;
	if (in.empty())
		return ret;
	size_t rows = 0;
	size_t cols = 0;
	for (const auto& e : in) {
		rows += e.rows();
		cols = e.cols();
	}
	ret.resize(rows, cols);
	size_t row_at = 0;
	for (size_t i = 0; i < in.size(); i++) {
		const auto& e = in[i];
		ret.block(row_at, 0, e.rows(), e.cols()) = e;
		row_at += e.rows();
	}
	return ret;
}

}  // anonymous namespace

namespace geokeyconf {

KeySampler::KeySampler(const std::string &env_fn, const std::string &rob_fn)
{
	pqpEnv_ = std::make_shared<PQP_Model>();
	pqpRob_ = std::make_shared<PQP_Model>();
	loadAll(pqpEnv_.get(), pqpRob_.get(),
	        env_fn, rob_fn);

	pqpEnv = pqpEnv_.get();
	pqpRobot = pqpRob_.get();
}

KeySampler::~KeySampler()
{
}

bool
KeySampler::isMyStateValid(Eigen::Quaterniond q, Eigen::Vector3d t)
{
	Eigen::Matrix3d R = q.toRotationMatrix();

	double tx = t[0];
	double ty = t[1];
	double tz = t[2];

	double robRot[3][3];
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			robRot[i][j] = R(i, j);
		}
	}

	double robT[3]      = {tx, ty, tz};
	double envT[3]      = {0, 0, 0};
	double envRot[3][3] = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};

	PQP_CollideResult cres;
	PQP_Collide(&cres, envRot, envT, pqpEnv, robRot, robT, pqpRobot, PQP_FIRST_CONTACT);
	if (cres.NumPairs() > 0) {
		return false;
	}
	if (cres.NumPairs() == 0) {
		return true;
	}
	return false;
}

bool
KeySampler::isMyStateValid(Eigen::Quaterniond q, Eigen::Vector3d t, double &distance)
{
	Eigen::Matrix3d R = q.toRotationMatrix();

	double tx = t[0];
	double ty = t[1];
	double tz = t[2];

	double robRot[3][3];
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			robRot[i][j] = R(i, j);
		}
	}

	double robT[3]      = {tx, ty, tz};
	double envT[3]      = {0, 0, 0};
	double envRot[3][3] = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};

	double rel_err = 0.0, abs_err = 0.0;
	PQP_DistanceResult dres;
	PQP_Distance(&dres, envRot, envT, pqpEnv, robRot, robT, pqpRobot, PQP_FIRST_CONTACT, rel_err, abs_err);
	distance = dres.Distance();
	if (distance > 0) {
		return true;
	}

	if (distance == 0) {
		return false;
	}
	return false;
}

bool
KeySampler::isMyStateValid(Eigen::Matrix3d R, Eigen::Vector3d robPV, Eigen::Vector3d envPV)
{
	Eigen::Quaterniond q(R);
	Eigen::Vector3d rotateV = R * robPV;
	Eigen::Vector3d T       = {envPV(0) - rotateV[0], envPV(1) - rotateV[1], envPV(2) - rotateV[2]};
	return isMyStateValid(q, T);
}

bool
KeySampler::isMyStateValid(Eigen::Matrix3d R, Eigen::Vector3d robPV, Eigen::Vector3d envPV, double &distance)
{
	Eigen::Quaterniond q(R);
	Eigen::Vector3d rotateV = R * robPV;
	Eigen::Vector3d T       = {envPV(0) - rotateV[0], envPV(1) - rotateV[1], envPV(2) - rotateV[2]};
	return isMyStateValid(q, T, distance);
}

bool
KeySampler::isMyStateValid(double angle1, double angle2, Eigen::Vector3d axis1, Eigen::Vector3d axis2,
                           Eigen::Matrix3d Ortho, Eigen::Vector3d robPV, Eigen::Vector3d envPV)
{
	Eigen::Matrix3d rot1;
	Eigen::Matrix3d rot2;
	rot1 = Eigen::AngleAxisd(angle1, axis1);
	rot2 = Eigen::AngleAxisd(angle2, axis2);
	return isMyStateValid(rot1 * rot2 * Ortho, robPV, envPV);
}

bool
KeySampler::isMyStateValid(double angle1, double angle2, Eigen::Vector3d axis1, Eigen::Vector3d axis2,
                           Eigen::Matrix3d Ortho, Eigen::Vector3d robPV, Eigen::Vector3d envPV, double &distance)
{
	Eigen::Matrix3d rot1;
	Eigen::Matrix3d rot2;
	rot1 = Eigen::AngleAxisd(angle1, axis1);
	rot2 = Eigen::AngleAxisd(angle2, axis2);
	return isMyStateValid(rot1 * rot2 * Ortho, robPV, envPV, distance);
}

bool
KeySampler::isMyStateValid(double angle1, Eigen::Vector3d axis1, Eigen::Matrix3d Onto, Eigen::Vector3d robPV,
                           Eigen::Vector3d envPV)
{
	Eigen::Matrix3d rot1;
	rot1 = Eigen::AngleAxisd(angle1, axis1);
	return isMyStateValid(rot1 * Onto, robPV, envPV);
}

bool
KeySampler::isMyStateValid(double angle1, Eigen::Vector3d axis1, Eigen::Matrix3d Onto, Eigen::Vector3d robPV,
                           Eigen::Vector3d envPV, double &distance)
{
	Eigen::Matrix3d rot1;
	rot1 = Eigen::AngleAxisd(angle1, axis1);
	return isMyStateValid(rot1 * Onto, robPV, envPV, distance);
}

std::tuple<Eigen::MatrixXd, Eigen::VectorXi, Eigen::VectorXi>
KeySampler::getAllKeyConfigs(Eigen::MatrixXd env_keypoints,
                             Eigen::MatrixXd rob_keypoints,
                             int number_of_rotations)
{
	std::vector<Eigen::MatrixXd> ret_confs;
	std::vector<Eigen::VectorXi> ret_envids;
	std::vector<Eigen::VectorXi> ret_robids;
	for (int i = 0; i < env_keypoints.rows(); i++) {
		for (int j = 0; j < rob_keypoints.rows(); j++) {
			Eigen::MatrixXd keys = getKeyConfigs(env_keypoints.row(i),
			                                     rob_keypoints.row(j),
			                                     number_of_rotations);
			if (keys.size() == 0)
				continue;
			ret_confs.emplace_back(keys);
			ret_envids.emplace_back(Eigen::MatrixXi::Constant(keys.rows(), 1, i));
			ret_robids.emplace_back(Eigen::MatrixXi::Constant(keys.rows(), 1, j));
		}
	}
	return std::make_tuple(cat(ret_confs),
	                       cat(ret_envids),
	                       cat(ret_robids));
}

Eigen::MatrixXd
KeySampler::getKeyConfigs(Eigen::VectorXd env,
                          Eigen::VectorXd rob,
                          int div1, int div2)
{
	double robPos = rob(6);
	double envPos = env(6);

#if 0
	std::string fname = "C:\\Users\\rbelfe2\\Desktop\\Motion Planning\\Logs\\" + puzzleName +
	                    "\\Valid Region\\Valid Indices" + std::to_string(id1) + "_" + std::to_string(id2) + ".txt";

#endif

	if (robPos <= 0.6 && envPos <= 0.6) {  // both outside
		return std::get<0>(getKeyConfigsBothOutside(env, rob, div1, div2));
	}

	Eigen::Vector3d robV1 = {rob(0), rob(1), rob(2)};
	Eigen::Vector3d robV2 = {rob(3), rob(4), rob(5)};
	Eigen::Vector3d envV1 = {env(0), env(1), env(2)};
	Eigen::Vector3d envV2 = {env(3), env(4), env(5)};

	double envDist = (envV1 - envV2).norm();
	double robDist = (robV1 - robV2).norm();

	Eigen::MatrixXd res1, res2;

	double thresh = getThreshNarrowTunnelRatio();
	if (robPos <= 0.6 && robDist > envDist && robDist < thresh * envDist) {  // rob outside
		res1 = std::get<0>(getKeyConfigsInOut(env, rob, div1, div2, 0));
		res2 = std::get<0>(getKeyConfigsInOut(env, rob, div1, div2, 1));
	} else if (envPos <= 0.6 && envDist > robDist && envDist < thresh * robDist) {  // env outside
		res1 = std::get<0>(getKeyConfigsInOut(env, rob, div1, div2, 0));
		res2 = std::get<0>(getKeyConfigsInOut(env, rob, div1, div2, 1));
	} else {
		return Eigen::MatrixXd();
	}
	if (res1.rows() == 0)
		return res2;
	if (res2.rows() == 0)
		return res1;
	Eigen::MatrixXd ret(res1.rows() + res2.rows(), res1.cols());
	ret << res1,
	       res2;
	return ret;
}

std::tuple<Eigen::MatrixXd, Eigen::VectorXi>
KeySampler::getKeyConfigsInOut(Eigen::VectorXd env,
                               Eigen::VectorXd rob,
                               int div1, int div2,
                               int rotationMode)
{
	Eigen::Vector3d robV1 = {rob(0), rob(1), rob(2)};
	Eigen::Vector3d robV2 = {rob(3), rob(4), rob(5)};
	Eigen::Vector3d envV1 = {env(0), env(1), env(2)};
	Eigen::Vector3d envV2 = {env(3), env(4), env(5)};

	Eigen::Vector3d envPV       = 0.5 * (envV1 + envV2);
	Eigen::Vector3d robPV       = 0.5 * (robV1 + robV2);
	Eigen::Vector3d robotOrient = robV2 - robV1;
	Eigen::Vector3d envOrient;
	if (rotationMode == 0) {
		envOrient = envV2 - envV1;
	} else {
		envOrient = envV1 - envV2;
	}
	Eigen::Matrix3d Onto = vectorRotate(robotOrient, envOrient);

	Eigen::Vector3d axis1 = envOrient.normalized();  // axis of rotation
	std::vector<Eigen::VectorXd> result;
	std::vector<double> selectedInds;
	const double PI = 3.141592653589793238463;
	bool valid      = false;
	std::map<double, bool> visited;

#if 0
	std::ofstream myFile1;
	myFile1.open(fname);
#endif

	std::vector<int> ret_indices;

	for (double i = 0; i < div1; i++) {
		Eigen::Matrix3d rot1;
		rot1                    = Eigen::AngleAxisd(2 * i * PI / div1, axis1);
		Eigen::Matrix3d R       = rot1 * Onto;
		Eigen::Vector3d rotateV = R * robPV;

		Eigen::Quaterniond q(R);
		Eigen::Vector3d T = {envPV(0) - rotateV[0], envPV(1) - rotateV[1], envPV(2) - rotateV[2]};
		double distance   = 0;
        if (disableStateValidation_) {
            Eigen::VectorXd temp(STATE_DIMENSION);
            temp(3) = q.x();
            temp(4) = q.y();
            temp(5) = q.z();
            temp(6) = q.w();
            temp(0) = T.x();
            temp(1) = T.y();
            temp(2) = T.z();
            result.emplace_back(temp);
            continue;
        }

		valid = isMyStateValid(q, T, distance);
		if ((visited.count(i) == 0) && (valid)) {
			double selectedRot = i;
			double maxDist     = distance;
			double step        = 2 * PI / (div1 * div2);
#if 0
			std::cout << "Found Valid State ";
			std::cout << T[0] << " " << T[1] << " " << T[2] << "\n"
			          << q.w() << " " << q.x() << " " << q.y() << " " << q.z() << std::endl;
			std::cout << i << " " << std::endl;
#endif
			double searchInd = i;
			ret_indices.emplace_back(searchInd);
#if 0
			myFile1 << searchInd << "\n";
#endif
			std::vector<double> region = {searchInd};

			while (visited.count(searchInd - 1 / div2) == 0 && searchInd - 1 / div2 >= 0 && valid) {
				searchInd          = searchInd - 1 / div2;
				visited[searchInd] = true;
				valid = isMyStateValid(2 * searchInd * PI / div1, axis1, Onto, robPV, envPV, distance);
				if (valid) {
					if (distance > maxDist) {
						maxDist     = distance;
						selectedRot = searchInd;
					}
					region.push_back(searchInd);
					ret_indices.emplace_back(searchInd);
#if 0
					myFile1 << searchInd << "\n";
#endif
				}
			}

			searchInd = i;
			while (visited.count(searchInd + 1 / div2) == 0 && searchInd + 1 / div2 < div1 && valid) {
				searchInd          = searchInd + 1 / div2;
				visited[searchInd] = true;
				valid = isMyStateValid(2 * searchInd * PI / div1, axis1, Onto, robPV, envPV, distance);
				if (valid) {
					if (distance > maxDist) {
						maxDist     = distance;
						selectedRot = searchInd;
					}
					region.push_back(searchInd);
					ret_indices.emplace_back(searchInd);
#if 0
					myFile1 << searchInd << "\n";
#endif
				}
			}
			visited[i]      = true;
			int randVal     = rand() % region.size();
			double selected = selectedRot;
			bool unique     = true;
			for (auto previous : selectedInds) {
				if (abs(selected - previous) < 5) {
					unique = false;
				}
			}
			if (unique) {
				selectedInds.push_back(selected);
				rot1              = Eigen::AngleAxisd(2 * PI * selected / div1, axis1);
				Eigen::Matrix3d R = rot1 * Onto;
				Eigen::Quaterniond q(R);
				Eigen::Vector3d rotateV = R * robPV;
				Eigen::Vector3d T       = {envPV(0) - rotateV[0], envPV(1) - rotateV[1],
                                                     envPV(2) - rotateV[2]};
				Eigen::VectorXd temp(STATE_DIMENSION);
				temp(3) = q.x();
				temp(4) = q.y();
				temp(5) = q.z();
				temp(6) = q.w();
				temp(0) = T.x();
				temp(1) = T.y();
				temp(2) = T.z();
				result.emplace_back(temp);
			}
		}
	}

#if 0
	myFile1.close();
	return result;
#endif
	return std::make_tuple(assembly(result), assembly(ret_indices));
}

std::tuple<Eigen::MatrixXd, Eigen::MatrixXi>
KeySampler::getKeyConfigsBothOutside(Eigen::VectorXd env,
                                     Eigen::VectorXd rob,
                                     int div1, int div2)
{
	Eigen::Vector3d robV1 = {rob(0), rob(1), rob(2)};
	Eigen::Vector3d robV2 = {rob(3), rob(4), rob(5)};
	Eigen::Vector3d envV1 = {env(0), env(1), env(2)};
	Eigen::Vector3d envV2 = {env(3), env(4), env(5)};

	Eigen::Vector3d robotOrient = robV2 - robV1;
	Eigen::Vector3d envOrient   = envV2 - envV1;
	double thresh = getThreshNarrowTunnelRatio();
	double robDist = robotOrient.norm();
	double envDist = envOrient.norm();
	// std::cerr << "robDist: " << robDist << " envDist: " << envDist << std::endl;
	if (robDist > thresh * envDist || envDist > thresh * robDist) {
		// The sizes of gaps should be close
		// Otherwise we shoud reject such pairs.
		return std::make_tuple(Eigen::MatrixXd(), Eigen::MatrixXi());
	}

	Eigen::Vector3d envPV       = 0.5 * (envV1 + envV2);
	Eigen::Vector3d robPV       = 0.5 * (robV1 + robV2);
	Eigen::Matrix3d Ortho       = orthoRotate(robotOrient, envOrient);

	Eigen::Vector3d axis1 = envOrient.normalized();              // axis 1 of rotation
	Eigen::Vector3d axis2 = (Ortho * robotOrient).normalized();  // axis 2 of rotation
	Eigen::Vector3d axis3 = axis1.cross(axis2);

	std::vector<Eigen::VectorXd> result;
	std::vector<Eigen::Vector2i> ret_indices;
	const double PI = 3.141592653589793238463;
	bool valid      = false;
	std::map<std::tuple<double, double>, bool> visited;
	std::vector<std::tuple<double, double>> selectedInds;

#if 0
	std::ofstream myFile1;
	myFile1.open(fname);
#endif

	for (int i = 0; i < div1; i++) {
		Eigen::Matrix3d rot1;
		rot1 = Eigen::AngleAxisd(2 * i * PI / div1, axis1);

		for (double j = 0; j < div1; j++) {
			Eigen::Matrix3d rot2 = Eigen::AngleAxisd((j * PI * 2) / div1, axis2).toRotationMatrix();
			Eigen::Matrix3d tempRot = rot1 * rot2 * Ortho;
			std::tuple<double, double> rotation(2 * i * PI / div1, 2 * j * PI / div1);
			std::tuple<double, double> indices(i, j);
            if (disableStateValidation_) {
                Eigen::Matrix3d R = rot1 * rot2 * Ortho;
                Eigen::Quaterniond q(R);
                Eigen::Vector3d rotateV = R * robPV;
                Eigen::Vector3d T       = {envPV(0) - rotateV[0], envPV(1) - rotateV[1],
                    envPV(2) - rotateV[2]};
                Eigen::VectorXd temp(STATE_DIMENSION);
                temp(3) = q.x();
                temp(4) = q.y();
                temp(5) = q.z();
                temp(6) = q.w();
                temp(0) = T.x();
                temp(1) = T.y();
                temp(2) = T.z();
                result.push_back(temp);
                continue;
            }
			for (double k = 0; k < 1; k++) {
				double distance         = 0;
				Eigen::Matrix3d R       = tempRot;
				Eigen::Vector3d rotateV = R * robPV;

				Eigen::Quaterniond q(R);
				Eigen::Vector3d T = {envPV(0) - rotateV[0], envPV(1) - rotateV[1],
				                     envPV(2) - rotateV[2]};
				double bound      = 0.00002;

				valid = isMyStateValid(q, T, distance);
				if ((visited.count(indices) == 0) && (valid)) {
					double step = 2 * PI / (div1 * div2);
#if 0
					std::cout << "Found Valid State ";
					std::cout << T[0] << " " << T[1] << " " << T[2] << "\n"
					          << q.w() << " " << q.x() << " " << q.y() << " " << q.z() << std::endl;
					std::cout << i << " " << j << std::endl;
#endif
					std::queue<std::tuple<double, double>> searchQueue;
					std::vector<std::tuple<double, double>> region;

					visited[indices] = true;
					searchQueue.push(indices);
					region.push_back(rotation);
					double maxDist = distance;
					std::pair<double, double> selectedRotation(2 * i * PI / div1,
					                                           2 * j * PI / div1);
					while (!searchQueue.empty()) {
						std::queue<std::tuple<double, double>> tempQueue;
						std::tuple<double, double> currInd = searchQueue.front();

#if 0
						myFile1 << std::get<0>(currInd) << " " << std::get<1>(currInd) << "\n";
#else
						Eigen::Vector2i ind;
						ind << std::get<0>(currInd) , std::get<1>(currInd);
						ret_indices.emplace_back(ind);
#endif
						// myFile2 <<
						// std::get<0>(currState) << " "
						// << std::get<1>(currState) <<
						// "\n";

						searchQueue.pop();
						std::tuple<double, double> up(
						        std::get<0>(currInd),
						        fmod(std::get<1>(currInd) + 1 / div2, div1));
						tempQueue.push(up);
						std::tuple<double, double> upl(
						        fmod(std::get<0>(currInd) - 1 / div2 + div1, div1),
						        fmod(std::get<1>(currInd) + 1 / div2, div1));
						tempQueue.push(upl);
						std::tuple<double, double> upr(
						        fmod(std::get<0>(currInd) + 1 / div2, div1),
						        fmod(std::get<1>(currInd) + 1 / div2, div1));
						tempQueue.push(upr);
						std::tuple<double, double> down(
						        std::get<0>(currInd),
						        fmod(std::get<1>(currInd) - 1 / div2 + div1, div1));
						tempQueue.push(down);
						std::tuple<double, double> downl(
						        fmod(std::get<0>(currInd) - 1 / div2 + div1, div1),
						        fmod(std::get<1>(currInd) - 1 / div2 + div1, div1));
						tempQueue.push(downl);
						std::tuple<double, double> downr(
						        fmod(std::get<0>(currInd) + 1 / div2, div1),
						        fmod(std::get<1>(currInd) - 1 / div2 + div1, div1));
						tempQueue.push(downr);
						std::tuple<double, double> left(
						        fmod(std::get<0>(currInd) - 1 / div2 + div1, div1),
						        std::get<1>(currInd));
						tempQueue.push(left);
						std::tuple<double, double> right(
						        fmod(std::get<0>(currInd) + 1 / div2, div1),
						        std::get<1>(currInd));
						tempQueue.push(right);
						while (!tempQueue.empty()) {
							currInd = tempQueue.front();
							std::tuple<double, double> index(std::get<0>(currInd),
							                                 std::get<1>(currInd));
							std::tuple<double, double> rot(
							        std::get<0>(currInd) * 2 * PI / div1,
							        std::get<1>(currInd) * 2 * PI / div1);
							tempQueue.pop();
							if (visited.count(index) == 0) {
								visited[index] = true;
								double x       = std::get<0>(rot);
								double y       = std::get<1>(rot);
								if (isMyStateValid(x, y, axis1, axis2, Ortho, robPV,
								                   envPV, distance) &&
								    x >= 0 && x < 2 * PI && y >= 0 && y < 2 * PI) {
									if (distance > maxDist) {
										maxDist          = distance;
										selectedRotation = std::make_pair(
										        std::get<0>(rot),
										        std::get<1>(rot));
									}
									searchQueue.push(currInd);
									region.push_back(rot);
								}
							}
						}
					}

					int random                         = rand() % region.size();
					std::pair<double, double> selected = selectedRotation;
					bool unique                        = true;
					for (auto previous : selectedInds) {
						if (pow(std::get<0>(selected) - std::get<0>(previous), 2) +
						            pow(std::get<1>(selected) - std::get<1>(previous), 2) <
						    100 * pow(2 * PI / div1, 2)) {
							unique = false;
						}
					}
					if (unique) {
						selectedInds.push_back(selected);
						rot1              = Eigen::AngleAxisd(std::get<0>(selected), axis1);
						rot2              = Eigen::AngleAxisd(std::get<1>(selected), axis2);
						Eigen::Matrix3d R = rot1 * rot2 * Ortho;
						Eigen::Quaterniond q(R);
						Eigen::Vector3d rotateV = R * robPV;
						Eigen::Vector3d T       = {envPV(0) - rotateV[0], envPV(1) - rotateV[1],
                                                                     envPV(2) - rotateV[2]};
						Eigen::VectorXd temp(STATE_DIMENSION);
						temp(3) = q.x();
						temp(4) = q.y();
						temp(5) = q.z();
						temp(6) = q.w();
						temp(0) = T.x();
						temp(1) = T.y();
						temp(2) = T.z();
						result.push_back(temp);
					}
				}
			}
		}
	}
#if 0
	myFile1.close();
	return result;
#endif
	return std::make_tuple(assembly(result), assembly(ret_indices));
}

}  // namespace geokeyconf
