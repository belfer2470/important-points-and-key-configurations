/**
 * SPDX-FileCopyrightText: Copyright © 2020 McGill University, The University of Texas at Austin
 * SPDX-FileContributor: Modified by Xinya Zhang <xinyazhang@utexas.edu>
 * SPDX-FileContributor: Robert Belfer <belfer2470@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
#include <cstdio>
#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>

#include "LoadModel.h"

namespace geokeyconf {

void
loadPQPModel(PQP_Model* model, Eigen::MatrixXd V, Eigen::MatrixXi F)
{
	int iter = 0;
	model->BeginModel();

	for (int faceInd = 0; faceInd < F.rows(); faceInd++) {
		int p1Ind, p2Ind, p3Ind;
		p1Ind = F(faceInd, 0);
		p2Ind = F(faceInd, 1);
		p3Ind = F(faceInd, 2);
		double p1[3], p2[3], p3[3];
		for (int i = 0; i < 3; i++) {
			p1[i] = V(p1Ind, i);
			p2[i] = V(p2Ind, i);
			p3[i] = V(p3Ind, i);
		}
		model->AddTri(p1, p2, p3, iter);
		iter++;
	}

	model->EndModel();
}

void
loadAll(PQP_Model* pqpEnv, PQP_Model* pqpRobot, const std::string& fEnv, const std::string& fRob)
{
	Eigen::MatrixXi Fenv;
	Eigen::MatrixXd Venv;

	Eigen::MatrixXi Frob;
	Eigen::MatrixXd Vrob;

	read_triangle_mesh(fEnv, Venv, Fenv);
	read_triangle_mesh(fRob, Vrob, Frob);
	loadPQPModel(pqpEnv, Venv, Fenv);
	loadPQPModel(pqpRobot, Vrob, Frob);
}

template <typename DerivedV, typename DerivedF>
bool
read_triangle_mesh(const std::string str, Eigen::PlainObjectBase<DerivedV>& V, Eigen::PlainObjectBase<DerivedF>& F)
{
	using namespace std;
	vector<vector<double> > vV, vN, vTC, vC;
	vector<vector<int> > vF, vFTC, vFN;

	FILE* fp = fopen(str.c_str(), "r");
	if (NULL == fp) {
		fprintf(stderr, "IOError: %s could not be opened...\n", str.c_str());
		return false;
	}

	// Annoyingly obj can store 4 coordinates, truncate to xyz for this
	// generic read_triangle_mesh
	bool success = readOBJ(fp, vV, vTC, vN, vF, vFTC, vFN);
	for (auto& v : vV) {
		v.resize(std::min(v.size(), (size_t)3));
	}
	V.resize(vV.size(), 3);
	F.resize(vF.size(), 3);
	for (int i = 0; i < vV.size(); i++) {
		for (int j = 0; j < 3; j++) {
			V(i, j) = vV.at(i).at(j);
		}
	}

	for (int i = 0; i < vF.size(); i++) {
		for (int j = 0; j < 3; j++) {
			F(i, j) = vF.at(i).at(j);
		}
	}
	return success;
}

template <typename Scalar, typename Index>
bool
readOBJ(FILE* obj_file, std::vector<std::vector<Scalar> >& V, std::vector<std::vector<Scalar> >& TC,
        std::vector<std::vector<Scalar> >& N, std::vector<std::vector<Index> >& F,
        std::vector<std::vector<Index> >& FTC, std::vector<std::vector<Index> >& FN)
{
	// File open was successful so clear outputs
	V.clear();
	TC.clear();
	N.clear();
	F.clear();
	FTC.clear();
	FN.clear();

	// variables and constants to assist parsing the .obj file
	// Constant strings to compare against
	std::string v("v");
	std::string vn("vn");
	std::string vt("vt");
	std::string f("f");
	std::string tic_tac_toe("#");

	const int IGL_LINE_MAX = 2048;

	char line[IGL_LINE_MAX];
	int line_no = 1;
	while (fgets(line, IGL_LINE_MAX, obj_file) != NULL) {
		char type[IGL_LINE_MAX];
		// Read first word containing type
		if (sscanf(line, "%s", type) == 1) {
			// Get pointer to rest of line right after type
			char* l = &line[strlen(type)];
			if (type == v) {
				std::istringstream ls(&line[1]);
				std::vector<Scalar> vertex{std::istream_iterator<Scalar>(ls),
				                           std::istream_iterator<Scalar>()};

				if (vertex.size() < 3) {
					fprintf(stderr,
					        "Error: readOBJ() vertex on "
					        "line %d should have at least "
					        "3 coordinates",
					        line_no);
					fclose(obj_file);
					return false;
				}

				V.push_back(vertex);
			} else if (type == vn) {
				double x[3];
				int count = sscanf(l, "%lf %lf %lf\n", &x[0], &x[1], &x[2]);
				if (count != 3) {
					fprintf(stderr,
					        "Error: readOBJ() normal on "
					        "line %d should have 3 "
					        "coordinates",
					        line_no);
					fclose(obj_file);
					return false;
				}
				std::vector<Scalar> normal(count);
				for (int i = 0; i < count; i++) {
					normal[i] = x[i];
				}
				N.push_back(normal);
			} else if (type == vt) {
				double x[3];
				int count = sscanf(l, "%lf %lf %lf\n", &x[0], &x[1], &x[2]);
				if (count != 2 && count != 3) {
					fprintf(stderr,
					        "Error: readOBJ() texture "
					        "coords on line %d should have "
					        "2 "
					        "or 3 coordinates (%d)",
					        line_no, count);
					fclose(obj_file);
					return false;
				}
				std::vector<Scalar> tex(count);
				for (int i = 0; i < count; i++) {
					tex[i] = x[i];
				}
				TC.push_back(tex);
			} else if (type == f) {
				const auto& shift   = [&V](const int i) -> int { return i < 0 ? i + V.size() : i - 1; };
				const auto& shift_t = [&TC](const int i) -> int {
					return i < 0 ? i + TC.size() : i - 1;
				};
				const auto& shift_n = [&N](const int i) -> int { return i < 0 ? i + N.size() : i - 1; };
				std::vector<Index> f;
				std::vector<Index> ftc;
				std::vector<Index> fn;
				// Read each "word" after type
				char word[IGL_LINE_MAX];
				int offset;
				while (sscanf(l, "%s%n", word, &offset) == 1) {
					// adjust offset
					l += offset;
					// Process word
					long int i, it, in;
					if (sscanf(word, "%ld/%ld/%ld", &i, &it, &in) == 3) {
						f.push_back(shift(i));
						ftc.push_back(shift_t(it));
						fn.push_back(shift_n(in));
					} else if (sscanf(word, "%ld/%ld", &i, &it) == 2) {
						f.push_back(shift(i));
						ftc.push_back(shift_t(it));
					} else if (sscanf(word, "%ld//%ld", &i, &in) == 2) {
						f.push_back(shift(i));
						fn.push_back(shift_n(in));
					} else if (sscanf(word, "%ld", &i) == 1) {
						f.push_back(shift(i));
					} else {
						fprintf(stderr,
						        "Error: readOBJ() face "
						        "on line %d has "
						        "invalid element "
						        "format\n",
						        line_no);
						fclose(obj_file);
						return false;
					}
				}
				if ((f.size() > 0 && fn.size() == 0 && ftc.size() == 0) ||
				    (f.size() > 0 && fn.size() == f.size() && ftc.size() == 0) ||
				    (f.size() > 0 && fn.size() == 0 && ftc.size() == f.size()) ||
				    (f.size() > 0 && fn.size() == f.size() && ftc.size() == f.size())) {
					// No matter what add each type to lists
					// so that lists are the correct lengths
					F.push_back(f);
					FTC.push_back(ftc);
					FN.push_back(fn);
				} else {
					fprintf(stderr,
					        "Error: readOBJ() face on line "
					        "%d has invalid format\n",
					        line_no);
					fclose(obj_file);
					return false;
				}
			} else if (strlen(type) >= 1 && (type[0] == '#' || type[0] == 'g' || type[0] == 's' ||
			                                 strcmp("usemtl", type) == 0 || strcmp("mtllib", type) == 0)) {
				// ignore comments or other shit
			} else {
				// ignore any other lines
				fprintf(stderr,
				        "Warning: readOBJ() ignored "
				        "non-comment line %d:\n  %s",
				        line_no, line);
			}
		} else {
			// ignore empty line
		}
		line_no++;
	}
	fclose(obj_file);

	assert(F.size() == FN.size());
	assert(F.size() == FTC.size());

	return true;
}
}  // namespace geokeyconf
