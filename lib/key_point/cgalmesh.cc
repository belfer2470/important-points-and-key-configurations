/**
 * SPDX-FileCopyrightText: Copyright © 2020 McGill University, The University of Texas at Austin
 * SPDX-FileContributor: Modified by Xinya Zhang <xinyazhang@utexas.edu>
 * SPDX-FileContributor: Robert Belfer <belfer2470@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
#include <vector>
#include <map>
#include <igl/point_mesh_squared_distance.h>

#include "cgalmesh.h"

namespace geokeyconf {

void
igl2cgal(const Eigen::MatrixXd& V,
         const Eigen::MatrixXi& F,
         Triangle_mesh& sm)
{
	using Mesh = Triangle_mesh;
	using Face_index = Mesh::Face_index;
	using Vertex_index = Mesh::Vertex_index;
	using size_type = Mesh::size_type;

	auto n = V.rows();
	auto fint = F.rows();
	auto e = n + fint;

	sm.reserve(sm.num_vertices() + n, sm.num_faces() + 2 * fint, sm.num_edges() + e);

	std::vector<Vertex_index> vertexmap(n);
	for (int i = 0; i < n; i++) {
		Vertex_index vi = sm.add_vertex(Point(V(i,0), V(i,1), V(i,2)));
		vertexmap[i] = vi;
	}

	std::vector<Vertex_index> vr(3);
	for (int i = 0; i < fint; i++) {
		for (std::size_t j = 0; j < 3; j++) {
			vr[j] = vertexmap[F(i, j)];
		}
		(void)sm.add_face(vr);
	}
}

#if 0 // Old version
std::vector<Point>
getSurfacePoints(const Skeleton& skeleton,
                 const Triangle_mesh& tmesh,
		 const Eigen::MatrixXd& V,
		 const Eigen::MatrixXi& F)
{
	Eigen::MatrixXd P(skeleton.vertex_set().size(), 3);
	for (int i = 0; i < skeleton.vertex_set().size(); i++) {
		P(i, 0) = skeleton[i].point.x();
		P(i, 1) = skeleton[i].point.y();
		P(i, 2) = skeleton[i].point.z();
	}

	Eigen::VectorXd sqrD;
	Eigen::VectorXi I;
	Eigen::MatrixXd C;

	std::vector<Point> result(skeleton.vertex_set().size());
	igl::point_mesh_squared_distance(P, V, F, sqrD, I, C);
	for (int i = 0; i < skeleton.vertex_set().size(); i++) {
		int FaceInd = I(i);
		Point temp(C(i,0),C(i,1), C(i,2));
		result[i] = temp;
	}
	return result;
}
#endif

Eigen::MatrixXd
getSurfacePoints(const Eigen::MatrixXd& intP,
                 const Triangle_mesh& tmesh,
                 const Eigen::MatrixXd& V,
                 const Eigen::MatrixXi& F)
{
	Eigen::VectorXd sqrD;
	Eigen::VectorXi I;
	Eigen::MatrixXd C;

	igl::point_mesh_squared_distance(intP, V, F, sqrD, I, C);
	return C;
}

std::vector<int>
getLocalMins(const Skeleton& skeleton, const Eigen::MatrixXd& intP, const Eigen::MatrixXd& surfP)
{
	enum {
		NOT_MINIMAL = 0,
		MINIMAL = 1,
		UNKNOWN = 2,
	};
	std::vector<int> localMins(skeleton.vertex_set().size(), MINIMAL);
	Eigen::VectorXd rwSqNorm = (intP - surfP).rowwise().squaredNorm();

	BOOST_FOREACH(Skeleton_edge e, edges(skeleton))
	{
		int s = source(e, skeleton);
		int t = target(e, skeleton);
#if 0
		double d1 = (intP.row(s) - surfP.row(s)).norm();
		double d2 = (intP.row(t) - surfP.row(t)).norm();
#else
		double d1 = rwSqNorm(s);
		double d2 = rwSqNorm(t);
#endif
		if (d1 < d2)
			localMins[t] = NOT_MINIMAL;
		if (d2 < d1)
			localMins[s] = NOT_MINIMAL;
	}
	return localMins;
}

std::vector<std::vector<int>>
getAdjacency(const Skeleton& skeleton)
{
	std::vector<std::vector<int>> result(skeleton.vertex_set().size());

	BOOST_FOREACH(Skeleton_edge e, edges(skeleton)) {
		int s = source(e, skeleton);
		int t = target(e, skeleton);
		result[s].emplace_back(t);
		result[t].emplace_back(s);
	}
	return result;
}

std::vector<int>
getMinGroups(const Skeleton& skeleton, const Eigen::MatrixXd& intP, const Eigen::MatrixXd& surfP,
             double local_tolerance_epsilon,
             double group_tolerance_epsilon)
{
	auto localMins = getLocalMins(skeleton, intP, surfP);	//indices of local minima
	auto NV = skeleton.vertex_set().size();
	std::vector<double> scores(NV);             //distance of point to surface
	std::vector<double> groupScores(NV);        //distance of closest poit of a set to surface
	std::vector<bool> visited(NV, false);   //dfs visited
	std::vector<std::pair<int,double>> minInds;		//index and value of closest points
	std::stack<int> dfs1;                   //dfs stack

	for (int i = 0; i < NV; i++) {
		scores[i] = (intP.row(i) - surfP.row(i)).norm();
		groupScores[i] = scores[i];
		if (localMins[i] == 1)
			minInds.emplace_back(i, scores[i]);
	}
	std::sort(minInds.begin(), minInds.end(), [](auto &left, auto &right) {
		return left.second < right.second;
	});

	auto adjacency = getAdjacency(skeleton);        //adjacency f the curve
	std::vector<std::vector<int>> minRegions(NV);   // map of local min ot its region
	std::vector<int> minRegionRoot(NV);             // Root of disjoint set

	for (int i = 0; i < NV; i++) {
		minRegions[i] = { i };
		minRegionRoot[i] = i;
	}

	double tolerance = 1.0 + local_tolerance_epsilon;

	for (const auto& minIndPair : minInds) {
		int minInd = minIndPair.first;
		if (visited[minInd])
			continue;
		dfs1.push(minInd);
		while (!dfs1.empty()) {
			int ind = dfs1.top();
			minRegionRoot[ind] = minInd;
			minRegions[minInd].push_back(ind);
			visited[ind] = true;
			dfs1.pop();
			for (auto neighborInd : adjacency[ind]) {
				if (visited[neighborInd])
					continue;
				if (scores[neighborInd] >= tolerance * groupScores[ind])
					continue;
				visited[neighborInd] == true;
				dfs1.push(neighborInd);
				groupScores[neighborInd] = groupScores[ind];
				localMins[neighborInd] = 1;
			}
		}
	}

	tolerance = 1.0 + group_tolerance_epsilon;

	BOOST_FOREACH(Skeleton_edge e, edges(skeleton)) {
		int s = source(e, skeleton);
		int t = target(e, skeleton);
#if 1
		if (tolerance * groupScores[s] < groupScores[t]) {	//s smaller than t -> delete t's region
			for (int i : minRegions[minRegionRoot[t]])
				localMins[i] = 0;
		}
		if (tolerance * groupScores[t] < groupScores[s]) { // t smaller than s -> delete t's region
			for (int i : minRegions[minRegionRoot[s]])
				localMins[i] = 0;
		}
#else
		if (tolerance  * scores[s] < scores[t] ||
		    tolerance * groupScores[s] < groupScores[t]) {
			// s smaller than t -> delete t's region
			for (int i : minRegions[minRegionRoot[t]])
				localMins[i] = 0;
		}
		if (tolerance * scores[t] < scores[s] ||
		    tolerance * groupScores[t] < groupScores[s]) {
			// t smaller than s -> delete t's region
			for (int i : minRegions[minRegionRoot[s]])
				localMins[i] = 0;
		}
#endif
	}

	return localMins;
}


Eigen::MatrixXd
adjustPoints(const igl::AABB<Eigen::MatrixXd, 3>& tree,
	     const Eigen::MatrixXd& intP,
	     const Eigen::MatrixXd& surfP,
	     const Eigen::MatrixXd& V,
	     const Eigen::MatrixXi& F)
{
	Eigen::MatrixXd res(intP.rows(), 3);
	res = 2 * intP - surfP;
	Eigen::VectorXd sqrD;
	Eigen::VectorXi I;
	Eigen::MatrixXd C;
	tree.squared_distance(V, F, res, sqrD, I, C);
	return C;
}

}
