/**
 * SPDX-FileCopyrightText: Copyright © 2020 McGill University, The University of Texas at Austin
 * SPDX-FileContributor: Modified by Xinya Zhang <xinyazhang@utexas.edu>
 * SPDX-FileContributor: Robert Belfer <belfer2470@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
#include <random>
#include <iostream>
#include <functional>

#include <igl/heat_geodesics.h>
#include <igl/avg_edge_length.h>
// #include <igl/readOBJ.h>
// #include <igl/exact_geodesic.h>
// #include <igl/delaunay_triangulation.h>
// #include <igl/edge_lengths.h>
// #include <igl/intrinsic_delaunay_triangulation.h>
#include <igl/read_triangle_mesh.h>
#include <igl/winding_number.h>

#include "../key_conf/LoadModel.h"
#include <KeyPoint.h>
#include <PointRecorder.h>

#include "geodesic_aux.h"
#include "cgalmesh.h"

//find pairs of important points on an object

//takes 2 arguments: a X.obj and number of iterations to look for important points. Will output them in as a text file XPoints.txt in the same disrectory as the obj file.

namespace geokeyconf {

Eigen::VectorXd
_getGeodesic(const igl::HeatGeodesicsData<double>* data, int vid)
{
	Eigen::VectorXd d = Eigen::VectorXd::Zero(data->Grad.cols(), 1);
	igl::heat_geodesics_solve(*data, (Eigen::VectorXi(1) << vid).finished(), d);
	return d*1/2;
}

void
getGeodesic(hmap &geodesic, std::map<int,int> &explored, igl::HeatGeodesicsData<double>* data, int select) {
	if (explored.count(select) == 0){
		Eigen::VectorXd d = _getGeodesic(data, select);
		explored[select] = 1;
		for (int i = 0; i < d.size(); i++) {
			geodesic[select][i] = d(i);
		}
	}
}

void
getGeodesic(hmap &geodesic, std::map<int, int> &explored, igl::HeatGeodesicsData<double>* data, std::vector<int> select) {
	for (int i : select) {
		getGeodesic(geodesic, explored, data, i);
	}
}

int getPair(int vid, Eigen::MatrixXd V, igl::HeatGeodesicsData<double>* data) {
	Eigen::VectorXd d = Eigen::VectorXd::Zero(data->Grad.cols(), 1);
	Eigen::VectorXd score = Eigen::VectorXd(d.size());
	igl::heat_geodesics_solve(*data, (Eigen::VectorXi(1) << vid).finished(), d);
	for (int i = 0; i < d.size(); i++) {
		if (i == vid) score(i) = 1;
		else score(i) = std::min(l2Dist(i, vid, V)/d(i),1.0);
	}

	int wid = minInd(score);
	return wid;
}

bool discreteIter(int &vid,
		  int &wid,
		  const std::vector<MyVertex> &graph,
		  const Eigen::MatrixXd &V,
		  std::map<int, int> &explored,
		  hmap &geodesic,
		  igl::HeatGeodesicsData<double>* data,
		  double alpha, double &currScore)
{
	double l2 = l2Dist(vid, wid, V);
	currScore = l2 / geodesic[vid][wid] + alpha * l2;
	std::vector<int> vAdj = graph[vid].adjacency;
	std::vector<int> wAdj = graph[wid].adjacency;
	vAdj.push_back(vid);
	wAdj.push_back(wid);


	for (int v : vAdj) {
		getGeodesic(geodesic, explored, data, v);

		for (int w : wAdj) {
			double templ2 = l2Dist(v, w, V);
			double tempScore;
			if (w == v) tempScore = 0;
			else tempScore = templ2 / geodesic[v][w] + alpha * templ2;
			if (tempScore < currScore) {
				vid = v;
				wid = w;
				currScore = tempScore;
				//std::cout << "discrete local search score: " << currScore << std::endl;
				//std::cout << "ratio: " << templ2 / geodesic[vid][wid] << std::endl << std::endl;
				return false;
			}
		}
	}
	return true;
}

bool contIter(int &fid,
	      int &gid,
	      const std::vector<MyVertex> &graph,
	      const Eigen::MatrixXd &V,
	      const Eigen::MatrixXi &F,
	      std::map<int, int> &explored,
	      hmap &geodesic,
	      igl::HeatGeodesicsData<double>* data,
	      double alpha,
          double &currScore,
	      Eigen::Vector3d &vf,
	      Eigen::Vector3d &wf,
          double &geodesic_distance
          )
{
	std::set<int> all1;
	std::set<int> all2;
	for (int i = 0; i < 3; i++) {
		all1.insert(graph[F(fid, i)].faces.begin(), graph[F(fid, i)].faces.end());
		all2.insert(graph[F(gid, i)].faces.begin(), graph[F(gid, i)].faces.end());
	}
	std::vector<int> fList(all1.begin(), all1.end());
	std::vector<int> gList(all2.begin(), all2.end());

	for (int f1 : fList) {
		int Aind = F(f1, 0);
		int Bind = F(f1, 1);
		int Cind = F(f1, 2);

		std::vector<int> tri = { Aind, Bind, Cind };

		getGeodesic(geodesic, explored, data, tri);

		for (int f2 : gList) {
			int Dind = F(f2, 0);
			int Eind = F(f2, 1);
			int Find = F(f2, 2);

			alglib::real_1d_array x;
			double _temp[] = { 0,0,0,0 };
			double param[] = { alpha, geodesic[Aind][Dind], geodesic[Aind][Eind], geodesic[Aind][Find],
					geodesic[Bind][Dind], geodesic[Bind][Eind], geodesic[Bind][Find],  geodesic[Cind][Dind], geodesic[Cind][Eind], geodesic[Cind][Find],
					V(Aind,0), V(Aind,1), V(Aind,2), V(Bind,0), V(Bind,1), V(Bind,2), V(Cind,0), V(Cind,1), V(Cind,2),
					V(Dind,0), V(Dind,1), V(Dind,2), V(Eind,0), V(Eind,1), V(Eind,2), V(Find,0), V(Find,1), V(Find,2) };
			x.setcontent(4, _temp);
			void* ptr = param;
			void(*rep2)(const alglib::real_1d_array &x, double func, void *ptr) = NULL;
			alglib::real_2d_array c = "[[1,0,0,0,0],[0,1,0,0,0], [-1,-1,0,0,-1], [0,0,1,0,0], [0,0,0,1,0], [0,0,-1,-1,-1]]";
			alglib::integer_1d_array ct = "[1,1,1,1,1,1]";
			alglib::minbleicstate state;
			alglib::minbleicreport rep;


			double epsg = 1e-10;
			double epsf = 0;
			double epsx = 0;
			alglib::ae_int_t maxits = 10;


			alglib::minbleiccreate(x, state);
			alglib::minbleicsetlc(state, c, ct);
			alglib::minbleicsetcond(state, epsg, epsf, epsx, maxits);
			alglib::minbleicoptimize(state, function1_grad, rep2, ptr);
			alglib::minbleicresults(state, x, rep);

			double optVal;
			alglib::real_1d_array grad;
			grad.setlength(4);
			function1_grad(x, optVal, grad, ptr);

			Eigen::Vector3d vtemp = x[0] * V.row(Aind) + x[1] * V.row(Bind) + (1 - x[0] - x[1]) * V.row(Cind);
			Eigen::Vector3d wtemp = x[2] * V.row(Dind) + x[3] * V.row(Eind) + (1 - x[2] - x[3]) * V.row(Find);
			optVal = sqrt(optVal);
			if (optVal < currScore) {
				fid = f1;
				gid = f2;
				currScore = optVal;
                double e = (vtemp - wtemp).norm();
                if (e == 0.0)
                    geodesic_distance = -1.0;
                else
                    geodesic_distance = e/ (currScore - alpha * e);
                std::cerr << "continuous score " << currScore << " alpha " << alpha << " e " << e << std::endl;
				//std::cout << "Continuous local search score " << optVal << std::endl;
				//std::cout << "ratio: " << ratio << std::endl << std::endl;
				vf = x[0] * V.row(Aind) + x[1] * V.row(Bind) + (1 - x[0] - x[1]) * V.row(Cind);
				wf = x[2] * V.row(Dind) + x[3] * V.row(Eind) + (1 - x[2] - x[3]) * V.row(Find);
				return false;
			}
		}
	}

	return true;
}

// FIXME: What does the "Iter" mean?
std::vector<Eigen::VectorXd>
findIter(const std::vector<MyVertex> &graph,
	 const Eigen::MatrixXd &V,
	 const Eigen::MatrixXi &F,
	 std::map<int, int> &explored,
	 hmap &geodesic,
	 igl::HeatGeodesicsData<double>* data,
	 double alpha,
	 std::function<int()> rng,
     std::shared_ptr<PointRecorder> recorder
     )
{
	Eigen::VectorXd ratio(V.rows());
	std::vector<Eigen::VectorXd> res;
	int vid = rng() % V.rows();
	getGeodesic(geodesic, explored, data, vid);
	// Initiall guessing
	std::vector<int> candidates = getLocalMins(graph, V, geodesic[vid], ratio, ratio, 0, vid, 0.9);
	if (candidates.empty()) {
		return std::vector<Eigen::VectorXd>();
	}
	int wid = rng() % candidates.size();
	wid = candidates.at(wid);

	double tunnelThresh = 0.2;

	bool local = false;
	bool cont1 = false;
	bool cont2 = false;
	bool cont3 = false;

	getGeodesic(geodesic, explored, data, wid);
	double currScore = -1.0;
	while (!local) {
		Eigen::Vector3d vOld = V.row(vid);
		Eigen::Vector3d wOld = V.row(wid);
        if (recorder) {
            // Should be safe since getGeodesic has been called at the
            // beginning of this function
            recorder->record_pair(vOld, wOld, geodesic[vid][wid], currScore);
        }
		Eigen::Vector3d delta = (wOld - vOld).normalized();
		local = discreteIter(vid, wid, graph, V, explored, geodesic, data, alpha, currScore);
		Eigen::Vector3d vNew = V.row(vid);
		Eigen::Vector3d wNew = V.row(wid);
		Eigen::Vector3d vDelt = vNew == vOld ?  vNew - vOld : (vNew-vOld).normalized();
		Eigen::Vector3d wDelt = wNew == wOld ? wNew - wOld : (wNew - wOld).normalized();

		if (std::abs(delta.dot(vDelt)) < tunnelThresh  && std::abs(delta.dot(wDelt)) < tunnelThresh) {
			Eigen::VectorXd pair(6);
			pair << vOld, wOld;
			res.push_back(pair);
		}
		else {
			res.clear();
		}


		//std::cout << "Discrete iter " << delta.dot(vDelt)<< " " << delta.dot(wDelt) << " " << (vNew - wNew).norm()<< std::endl;
	}

	if (vid == wid || std::find(graph[vid].adjacency.begin(), graph[vid].adjacency.end(), wid) != graph[vid].adjacency.end()) {
		cont2 = true;
		cont3 = true;
		vid = wid;
	}
	Eigen::Vector3d wf = V.row(wid);

	int fid = graph[vid].faces[0];
	int gid = graph[wid].faces[0];
	Eigen::Vector3d vf = V.row(vid);
    double geodesic_distance = geodesic[vid][wid];

	while (!cont2) {
		Eigen::Vector3d vOld = vf;
		Eigen::Vector3d wOld = wf;
        if (recorder) {
            recorder->record_pair(vOld, wOld, geodesic_distance, currScore);
            std::cerr << "continuous (2) geodesic_distance " << geodesic_distance << std::endl;
        }
		Eigen::Vector3d delta = (wOld - vOld).normalized();
		cont2 = contIter(fid, gid, graph, V, F, explored, geodesic, data, alpha,
                         currScore, vf, wf, geodesic_distance);
		Eigen::Vector3d vDelt = vf == vOld ? vf - vOld : (vf - vOld).normalized();
		Eigen::Vector3d wDelt = wf == wOld ? vf - wOld : (wf - wOld).normalized();

		if (std::abs(delta.dot(vDelt)) < tunnelThresh  && std::abs(delta.dot(wDelt)) < tunnelThresh) {
			Eigen::VectorXd pair(6);
			pair << vOld, wOld;
			res.push_back(pair);
		}
		else {
			res.clear();
		}
	}

	while (!cont3) {
		Eigen::Vector3d vOld = vf;
		Eigen::Vector3d wOld = wf;
        if (recorder) {
            recorder->record_pair(vOld, wOld, geodesic_distance, currScore);
            std::cerr << "continuous (3) geodesic_distance " << geodesic_distance << std::endl;
        }
		Eigen::Vector3d delta = (wOld - vOld).normalized();
		cont3 = contIter(fid, gid, graph, V, F, explored, geodesic, data, 0,
                         currScore, vf, wf, geodesic_distance);
		Eigen::Vector3d vDelt = vf == vOld ? vf - vOld : (vf - vOld).normalized();
		Eigen::Vector3d wDelt = wf == wOld ? vf - wOld : (wf - wOld).normalized();
		//std::cout << "Cont3 iter " << delta.dot(vDelt) << " " << delta.dot(wDelt) << " " << (vf - wf).norm() << std::endl;

		if (std::abs(delta.dot(vDelt)) < tunnelThresh  && std::abs(delta.dot(wDelt)) < tunnelThresh) {
			Eigen::VectorXd pair(6);
			pair << vOld, wOld;
			res.push_back(pair);
		}
		else {
			res.clear();
		}

	}

	Eigen::VectorXd pair(6);
	pair << vf , wf;
	res.push_back(pair);
    if (recorder)
        recorder->record_pair(vf, wf, geodesic_distance, currScore);

	if (res.size() <= 3) {
		res = { res.at(res.size() - 1) };
	}

	if (res.size() > 3) {
		res = {res.at(0), res.at(res.size()/4) , res.at(3 * res.size() /4), res.at(res.size() - 1) };

	}

	return res;
}

struct KeyPointProber::InternalData {
	hmap geodesic;
	std::vector<MyVertex> graph;
	igl::HeatGeodesicsData<double> heat_data;
	// double thresh = 0.8; // not used by main()
	Valuation dir = Heuristic;

	std::map<int, int> explored;
	std::map <Eigen::Vector3d, std::map<Eigen::Vector3d, int, VectorComp>, VectorComp> samples;
	Eigen::MatrixXd V;
	Eigen::MatrixXi F;

	Triangle_mesh tmesh;

	bool mcs_initialized = false; // Mean curvature skeleton
	Skeleton skeleton;
	igl::AABB<Eigen::MatrixXd, 3> aabb_tree;

	void init(Eigen::MatrixXd inV,
	          Eigen::MatrixXi inF)
	{
		V = std::move(inV);
		F = std::move(inF);
		igl2cgal(V, F, tmesh);

		generateGraph(graph, V, F);
		heat_data.use_intrinsic_delaunay = true;
		double t = std::pow(igl::avg_edge_length(V, F), 2);
		igl::heat_geodesics_precompute(V, F, heat_data);

		aabb_tree.init(V, F);
	}

	// mcs: mean curvature skeleton
	void init_mcs()
	{
		if (mcs_initialized)
			return ;

		Skeletonization mcs(tmesh);
		mcs.contract_until_convergence();
		mcs.set_quality_speed_tradeoff(100);
		mcs.set_medially_centered_speed_tradeoff(1000);
		mcs.convert_to_skeleton(skeleton);

		mcs_initialized = true;
	}

	std::vector<std::map<int, Eigen::MatrixXd>> intermediate_data;

	void record_ID(const Eigen::MatrixXd& P1,
	               const Eigen::MatrixXd& P2,
	               const Eigen::MatrixXd& P3)
	{
		std::map<int, Eigen::MatrixXd> m;
		m[0] = P1;
		m[1] = P2;
		m[2] = P3;
		intermediate_data.emplace_back(std::move(m));
#if 0
		auto& b = intermediate_data.back();
		b[0] = P1;
		b[1] = P2;
		b[2] = P3;
#endif
	}

	//
	// Returns hit distance and the mid point till the hit
	//
	// Note we do need a copy of P1 and P2 so we can modify them
	std::tuple<Eigen::VectorXi, Eigen::MatrixXd>
	line_segment_intersect(Eigen::MatrixXd P1,
	                       Eigen::MatrixXd P2,
	                       double epsilon = 1e-6)
	{
		Eigen::MatrixXd v12 = (P2 - P1);
		Eigen::VectorXd ds = v12.rowwise().norm();
		v12.rowwise().normalize();
		// Move P1 and P2 off the mesh surface
		P1 += epsilon * v12;
		P2 -= epsilon * v12;

		int N = P1.rows();
		Eigen::VectorXi ret1(N);
		for (int i = 0; i < N; i++) {
			igl::Hit hit;
			if (!aabb_tree.intersect_ray(V, F, P1.row(i), v12.row(i), hit))
				continue;
			if (hit.t < ds(i) - 2 * epsilon) {
				ret1(i) = hit.t + epsilon;
				P1.row(i) += v12.row(i) * (0.5 * hit.t);
			} else {
				ret1(i) = -1.0;
			}
		}
		return std::make_tuple(ret1, P1);
	}
};

KeyPointProber::KeyPointProber(const std::string& model_fn)
{
	d_ = std::make_shared<InternalData>();
	Eigen::MatrixXd V;
	Eigen::MatrixXi F;
    igl::read_triangle_mesh(model_fn, V, F);
	d_->init(std::move(V), std::move(F));
}

KeyPointProber::~KeyPointProber()
{
}

Eigen::MatrixXd
KeyPointProber::probeKeyPoints(int attempts, int seed, std::shared_ptr<PointRecorder> recorder)
{
	std::vector<Eigen::VectorXd> ret;
	auto& samples = d_->samples;
	std::mt19937 gen;
	if (seed == 0) {
		gen.seed(std::random_device()());
	} else {
		gen.seed(seed);
	}
	std::uniform_int_distribution<int> dist;
	auto rng = [&dist, &gen]() -> int {
		return dist(gen);
	};

	for (int i = 0; i < attempts; i++) {
		auto pairs = findIteratively(rng, recorder);
		for (const auto& pair : pairs) {
			Eigen::Vector3d Point1 = { pair(0), pair(1), pair(2) };
			Eigen::Vector3d Point2 = { pair(3), pair(4), pair(5) };
			if ((samples.count(Point1) == 0 ||
			     samples[Point1].count(Point2) == 0) &&
			    (samples.count(Point2) == 0 ||
			     samples[Point2].count(Point1) == 0)) {
				samples[Point1][Point2] = 1;
				samples[Point2][Point1] = 1;
				Eigen::MatrixXd P1(1, 3);
				Eigen::MatrixXd P2(1, 3);
				P1 << pair(0), pair(1), pair(2);
				P2 << pair(3), pair(4), pair(5);

				// if (P1(0) != P2(0) || P1(1) != P2(1) || P1(2) != P2(2)) {
				if ((P1-P2).squaredNorm() == 0) // Identical points
					continue ;

				auto tup = d_->line_segment_intersect(P1, P2);
				double hit = std::get<0>(tup)(0);
				/*
 				 * We need winding numbers to distinguish two
				 * cases:
				 * 1. Gap, used for all wired puzzles
				 * 2. Solid part of the mesh, used for the
				 *    threaded bolt
				 */
				Eigen::VectorXd W; // temp. variable for winding number
				if (hit >= 0) {
					// P1-P2 intersect with the mesh
					igl::winding_number(d_->V, d_->F, std::get<1>(tup), W);
					if (W(0) < 0.6) {
						// This is supposed to be a gap, but a gap should not have obstacles in the middle.
						// Reject.
						continue ;
					}
				}

				Eigen::MatrixXd Pmid(1, 3);
				Pmid = (P1 + P2) * 0.5;
				// Pmid << (P1(0) + P2(0)) / 2, (P1(1) + P2(1)) / 2, (P1(2) + P2(2)) / 2;
				igl::winding_number(d_->V, d_->F, Pmid, W);
				// std::cerr << "winding_number " << W(0) << std::endl;
				// myfile << P1(0) << " " << P1(1) << " " << P1(2) << " " << P2(0) << " " << P2(1) << " " << P2(2) << " " << W(0) << "\n";
				Eigen::VectorXd line;
				line.resize(7, 1);
				line << P1(0), P1(1), P1(2), P2(0), P2(1), P2(2), W(0);
				ret.emplace_back(line);
			}
		}
	}
	Eigen::MatrixXd e3ret;
	if (ret.empty())
		return e3ret;
	e3ret.resize(ret.size(), ret.front().rows());
	for (size_t i = 0; i < ret.size(); i++) {
		e3ret.row(i) = ret[i];
	}
	return e3ret;
}

Eigen::MatrixXd
KeyPointProber::probeNotchPoints(unsigned long seed, bool keepIntermediateData)
{
	if (seed != 0) {
		srand(seed);
		srand48(seed);
	}
	d_->init_mcs();

	double localMinThresh = getLocalMinThresh();
	const auto& V = d_->V;
	const auto& F = d_->F;
	const auto& tmesh = d_->tmesh;
	const auto& skeleton = d_->skeleton;
	const auto& aabb_tree = d_->aabb_tree;

	const auto NV = skeleton.vertex_set().size();
	std::vector<Eigen::VectorXd> ret;
	Eigen::MatrixXd P1(NV, 3);
	Eigen::MatrixXd P2(NV, 3);
	Eigen::MatrixXd P3(NV, 3);

	for (int i = 0; i < NV; i++) {
		P1.row(i) = getSkeletonPoint(i);
	}

	P2 = getSurfacePoints(P1, tmesh, V, F);
	P3 = adjustPoints(aabb_tree, P1, P2, V, F);
	if (keepIntermediateData) {
		d_->record_ID(P1, P2, P3);
	}
	P1 = (P2 + P3) / 2;
	P2 = getSurfacePoints(P1, tmesh, V, F);
	P3 = adjustPoints(aabb_tree, P1, P2, V, F);
	if (keepIntermediateData) {
		d_->record_ID(P1, P2, P3);
	}
	P1 = (P2 + P3) / 2;

	auto localMins = getMinGroups(skeleton, P1, P2,
	                              localMinThresh,
				      tolerance_epsilon_);
	if (keepIntermediateData) {
		auto lmm = getLocalMins(skeleton, P1, P2);
		std::vector<int> lm;
		for (size_t i = 0; i < lmm.size(); i++)
			if (lmm[i] == 1)
				lm.emplace_back(i);
		Eigen::MatrixXd lmP1(lm.size(), 3);
		Eigen::MatrixXd lmP2(lm.size(), 3);
		Eigen::MatrixXd lmP3(lm.size(), 3);
		for (size_t i = 0; i < lm.size(); i++) {
			lmP1.row(i) = P1.row(lm[i]);
			lmP2.row(i) = P2.row(lm[i]);
			lmP3.row(i) = P3.row(lm[i]);
		}
		d_->record_ID(lmP1, lmP2, lmP3);
	}

	for (int i = 0; i < skeleton.vertex_set().size(); i++) {
		if (localMins[i] != 1)
			continue;
		Eigen::VectorXd line;
		line.resize(7, 1);
		line << P3(i,0), P3(i,1), P3(i,2), P2(i,0), P2(i,1), P2(i,2), 1.0;
		ret.emplace_back(line);
	}

	Eigen::MatrixXd e3ret;
	if (ret.empty())
		return e3ret;
	e3ret.resize(ret.size(), ret.front().rows());
	for (size_t i = 0; i < ret.size(); i++) {
		e3ret.row(i) = ret[i];
	}
	return e3ret;
}

Eigen::MatrixXd
KeyPointProber::getIntermediateData(int iteration, int key) const
{
	if (iteration >= d_->intermediate_data.size())
		return Eigen::MatrixXd();
	const std::map<int, Eigen::MatrixXd>& m = d_->intermediate_data[iteration];
	auto iter = m.find(key);
	if (iter == m.end())
		return Eigen::MatrixXd();
	return iter->second;
}

std::tuple<Eigen::MatrixXd, Eigen::MatrixXd, Eigen::VectorXd, Eigen::VectorXd>
KeyPointProber::getLocalMinFromFace(int select, double thresh) const
{
	const auto& V = getV();
	Eigen::VectorXd ratio(V.rows());
	Eigen::VectorXd score(V.rows());
	getGeodesic(d_->geodesic, d_->explored, &d_->heat_data, select);

	auto localMins = getLocalMins(d_->graph, V, d_->geodesic[select],
				      ratio, score,
				      getAlpha(), select, thresh);
	Eigen::MatrixXd P2(localMins.size(), 3);
	Eigen::MatrixXd P3(localMins.size(), 3);

	for (size_t i = 0 ; i < localMins.size();i++) {
		P2.row(i) = V.row(localMins[i]);
		P3.row(i) = (V.row(select) + V.row(localMins[i])) / 2.0;
	}
	return std::make_tuple(P2, P3, ratio, score);
}

const hmap&
KeyPointProber::getCurrentHeatMap() const
{
	return d_->geodesic;
}

std::vector<Eigen::VectorXd>
KeyPointProber::findIteratively(std::function<int()> rng,
                                std::shared_ptr<PointRecorder> recorder)
{
	return findIter(d_->graph, d_->V, d_->F,
	                d_->explored, d_->geodesic,
	                &d_->heat_data,
	                alpha_,
	                rng,
                    recorder);
}

const Eigen::MatrixXd& KeyPointProber::getV() const
{
	return d_->V;
}

const Eigen::MatrixXi& KeyPointProber::getF() const
{
	return d_->F;
}

int
KeyPointProber::getSkeletonSize() const
{
	d_->init_mcs();
	return static_cast<int>(d_->skeleton.vertex_set().size());
}

Eigen::Vector3d
KeyPointProber::getSkeletonPoint(int index) const
{
	d_->init_mcs();

	Eigen::Vector3d ret;
	const auto p = d_->skeleton[index].point;
	ret << p.x(), p.y(), p.z();
	return ret;
}

Eigen::MatrixXd
KeyPointProber::getAllSkeletonPoints() const
{
	Eigen::MatrixXd ret;
	auto N = getSkeletonSize();
	ret.resize(N, 3);
	for (int i = 0; i < N; i++) {
		const auto& p = d_->skeleton[i].point;
		ret.row(i) << p.x(), p.y(), p.z();
	}
	return ret;
}

Eigen::MatrixXd
KeyPointProber::getSkeletonVertexSet(int index) const
{
	d_->init_mcs();

	const auto verts = d_->skeleton[index].vertices;
	Eigen::MatrixXd P2(verts.size(), 3);
	for (int i = 0; i < P2.rows(); i++) {
		Point temp = get(CGAL::vertex_point, d_->tmesh, verts[i]);
		P2(i, 0) = temp.x();
		P2(i, 1) = temp.y();
		P2(i, 2) = temp.z();
	}
	return P2;
}

Eigen::MatrixXi
KeyPointProber::getSkeletonEdges() const
{
	d_->init_mcs();

	std::vector<int> S;
	std::vector<int> T;

	// FIXME: better way to extract the edges
	BOOST_FOREACH(Skeleton_edge e, edges(d_->skeleton))
	{
		S.emplace_back(source(e, d_->skeleton));
		T.emplace_back(target(e, d_->skeleton));
	}

	Eigen::MatrixXi E(S.size(), 2);
	for (int i = 0; i < E.rows(); i++) {
		E.row(i) << S[i], T[i];
	}
	return E;
}

void
KeyPointProber::reset()
{
	d_->samples.clear();
}

};
