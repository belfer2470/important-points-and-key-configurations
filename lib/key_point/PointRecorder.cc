/**
 * SPDX-FileCopyrightText: Copyright © 2020 McGill University, The University of Texas at Austin
 * SPDX-FileContributor: Modified by Xinya Zhang <xinyazhang@utexas.edu>
 * SPDX-FileContributor: Robert Belfer <belfer2470@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
#include <PointRecorder.h>

namespace geokeyconf {

PointRecorder::PointRecorder()
{
}
    
PointRecorder::~PointRecorder()
{
}

void PointRecorder::record_pair(const Eigen::Vector3d& v, const Eigen::Vector3d& w,
                                double geodesic_distance, double score)
{
    pairs_.emplace_back(std::make_tuple(v, w, geodesic_distance, score));
}

Eigen::MatrixXd PointRecorder::report_pairs() const
{
    Eigen::MatrixXd ret;
    ret.resize(pairs_.size(), 8);
    for (size_t i = 0 ; i < pairs_.size(); i++) {
        const Eigen::Vector3d &v = std::get<0>(pairs_[i]);
        const Eigen::Vector3d &w = std::get<1>(pairs_[i]);
        double d = std::get<2>(pairs_[i]);
        double s = std::get<3>(pairs_[i]);
        ret.row(i) << v(0), v(1), v(2), w(0), w(1), w(2), d, s;
    }
    return ret;
}

}
