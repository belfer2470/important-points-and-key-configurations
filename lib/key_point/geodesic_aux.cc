/**
 * SPDX-FileCopyrightText: Copyright © 2020 McGill University, The University of Texas at Austin
 * SPDX-FileContributor: Modified by Xinya Zhang <xinyazhang@utexas.edu>
 * SPDX-FileContributor: Robert Belfer <belfer2470@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
#include <Eigen/Geometry>
#include "geodesic_aux.h"

namespace geokeyconf {

bool VectorComp::operator() (const Eigen::Vector3d &lhs, const Eigen::Vector3d &rhs) const
{
	if (lhs(0) == rhs(0)) {
		if (lhs(1) == rhs(1)) {
			return lhs(2) < rhs(2);
		}
		return lhs(1) < rhs(1);
	}
	return lhs(0) < rhs(0);
}

int maxInd(const Eigen::VectorXf& vec)
{
	float best = 0;
	int res = 0;
	for (int j = 0; j < vec.size(); j++) {
		if (vec[j] > best) {
			best = vec[j];
			res = j;
		}
	}
	return res;
}

int minInd(const Eigen::VectorXd& vec)
{
	double best = 1;
	int res = 0;
	for (int j = 0; j < vec.size(); j++) {
		if (vec(j) < best) {
			best = vec(j);
			res = j;
		}
	}
	return res;
}

int minInd(const Eigen::VectorXf& vec)
{
	float best = 1;
	int res = 0;
	for (int j = 0; j < vec.size(); j++) {
		if (vec(j) < best) {
			best = vec(j);
			res = j;
		}
	}
	return res;
}

int minInd(const Eigen::Vector3d& vec)
{
	float best = 2;
	int res = 0;
	for (int j = 0; j < 3; j++) {
		if (vec(j) < best) {
			best = vec(j);
			res = j;
		}
	}
	return res;
}


Eigen::Vector3d Project(const Eigen::Vector3d &q,
			const Eigen::Vector3d &A,
			const Eigen::Vector3d &B,
			const Eigen::Vector3d &C)
{
	Eigen::Vector3d res;

	Eigen::Vector3d n = ((A - B).cross(A - C)).normalized();
	Eigen::Vector3d v = q - A;
	double dist = v.dot(n);
	res = q - n * dist;
	return res;
}

Eigen::Vector3d nearest(const Eigen::Vector3d &q,
			const Eigen::Vector3d &A,
			const Eigen::Vector3d &B,
			const Eigen::Vector3d &C)
{
	Eigen::Vector3d v0 = B - A;
	Eigen::Vector3d v1 = C - A;
	Eigen::Vector3d v2 = C - B;
	Eigen::Vector3d v3 = q - A;
	Eigen::Vector3d v4 = q - B;
	Eigen::Vector3d v5 = q - C;
	double d00 = v0.dot(v0);
	double d01 = v0.dot(v1);
	double d11 = v1.dot(v1);
	double d30 = v3.dot(v0);
	double d31 = v3.dot(v1);
	double denom = d00 * d11 - d01 * d01;
	double v = (d11 * d30 - d01 * d31) / denom;
	double w = (d00 * d31 - d01 * d30) / denom;
	double u = 1.0 - v - w;

	if (v >= 0 && u >= 0 && w >= 0) {
		Eigen::Vector3d res(v, w, u);
		return res;
	}

	double d22 = v2.dot(v2);

	double t1 = std::max(0.0, std::min(1.0, v3.dot(v0) / d00));
	Eigen::Vector3d projection1 = A + t1 * v0;
	double t2 = std::max(0.0, std::min(1.0, v3.dot(v1) / d11));
	Eigen::Vector3d projection2 = A + t2 * v1;
	double t3 = std::max(0.0, std::min(1.0, v3.dot(v2) / d22));
	Eigen::Vector3d projection3 = B + t3 * v2;

	double dist1 = (q - projection1).norm();
	double dist2 = (q - projection2).norm();
	double dist3 = (q - projection3).norm();

	if (dist1 <= dist2 && dist1 <= dist3) {
		Eigen::Vector3d res(1 - t1, t1, 0.0);
		return res;
	}
	if (dist2 <= dist1 && dist2 <= dist3) {
		Eigen::Vector3d res(1 - t2, 0.0, t2);
		return res;
	}

	Eigen::Vector3d res(1 - t3, 0.0, t3);
	return res;
}

void function1_grad(const alglib::real_1d_array &x,
                    double &func,
                    alglib::real_1d_array &grad,
                    void *ptr)
{
	double *par = static_cast<double*>(ptr);

	double alpha = *par;

	Eigen::Vector3d A = { *(par + 10), *(par + 11), *(par + 12) };
	Eigen::Vector3d B = { *(par + 13), *(par + 14), *(par + 15) };
	Eigen::Vector3d C = { *(par + 16), *(par + 17), *(par + 18) };
	Eigen::Vector3d D = { *(par + 19), *(par + 20), *(par + 21) };
	Eigen::Vector3d E = { *(par + 22), *(par + 23), *(par + 24) };
	Eigen::Vector3d F = { *(par + 25), *(par + 26), *(par + 27) };

	Eigen::Vector3d V = x[0] * A + x[1] * B + (1 - x[0] - x[1]) * C;
	Eigen::Vector3d W = x[2] * D + x[3] * E + (1 - x[2] - x[3]) * F;
	Eigen::Matrix3d G;

	G <<  *(par +1), *(par +2), *(par + 3), *(par + 4), *(par + 5), *(par + 6), *(par + 7), *(par + 8), *(par + 9);

	Eigen::Vector3d a1 = { x[0], x[1], 1 - x[0] - x[1] };
	Eigen::Vector3d a2 = { x[2], x[3], 1 - x[2] - x[3] };

	Eigen::Vector3d dadx = { 1, 0, -1 };
	Eigen::Vector3d dady = { 0, 1, -1 };


	double geo = a1.dot(G * a2);

	double dg1 = dadx.dot(G * a2);
	double dg2 = dady.dot(G * a2);
	double dg3 = a1.dot(G * dadx);
	double dg4 = a1.dot(G * dady);

	double e2 = (V-W).squaredNorm();
	if (geo == 0) {
		func = 1;
		grad[0] = 0;
		grad[1] = 0;
		grad[2] = 0;
		grad[3] = 0;
	}
	else {
		func = e2  * std::pow(alpha + 1/(geo*geo), 2);

		double de1 = 2 * (A - C).dot(V - W);
		double de2 = 2 * (B - C).dot(V - W);
		double de3 = 2 * (D - F).dot(W - V);
		double de4 = 2 * (E - F).dot(W - V);

		grad[0] = std::pow(1 / geo + alpha, 2)*de1 - 2 * (1 / geo + alpha) * e2 / (geo * geo) * dg1;
		grad[1] = std::pow(1 / geo + alpha, 2)*de2 - 2 * (1 / geo + alpha) * e2 / (geo * geo) * dg2;
		grad[2] = std::pow(1 / geo + alpha, 2)*de3 - 2 * (1 / geo + alpha) * e2 / (geo * geo) * dg3;
		grad[3] = std::pow(1 / geo + alpha, 2)*de4 - 2 * (1 / geo + alpha) * e2 / (geo * geo) * dg4;
	}
	//std::cout << "grads: " << grad[0] << " " << grad[1] << " " << grad[2] << " " << grad[3] << std::endl;
}

void generateGraph(std::vector<MyVertex> &graph,
                   const Eigen::MatrixXd &V,
                   const Eigen::MatrixXi &F)
{
	for (int i = 0; i < V.rows(); i++) {
		MyVertex v = MyVertex();
		v.x = V(i, 0);
		v.y = V(i, 1);
		v.z = V(i, 2);
		graph.push_back(v);
	}

	for (int i = 0; i < F.rows(); i++) {
		if (std::find(graph[F(i, 0)].adjacency.begin(), graph[F(i, 0)].adjacency.end(), F(i, 1)) == graph[F(i, 0)].adjacency.end()) {
			graph[F(i, 0)].adjacency.push_back(F(i, 1));
		}
		if (std::find(graph[F(i, 0)].adjacency.begin(), graph[F(i, 0)].adjacency.end(), F(i, 2)) == graph[F(i, 0)].adjacency.end()) {
			graph[F(i, 0)].adjacency.push_back(F(i, 2));
		}
		graph[F(i, 0)].faces.push_back(i);

		if (std::find(graph[F(i, 1)].adjacency.begin(), graph[F(i, 1)].adjacency.end(), F(i, 0)) == graph[F(i, 1)].adjacency.end()) {
			graph[F(i, 1)].adjacency.push_back(F(i, 0));
		}
		if (std::find(graph[F(i, 1)].adjacency.begin(), graph[F(i, 1)].adjacency.end(), F(i, 2)) == graph[F(i, 1)].adjacency.end()) {
			graph[F(i, 1)].adjacency.push_back(F(i, 2));
		}
		graph[F(i, 1)].faces.push_back(i);

		if (std::find(graph[F(i, 2)].adjacency.begin(), graph[F(i, 2)].adjacency.end(), F(i, 0)) == graph[F(i, 2)].adjacency.end()) {
			graph[F(i, 2)].adjacency.push_back(F(i, 0));
		}
		if (std::find(graph[F(i, 2)].adjacency.begin(), graph[F(i, 2)].adjacency.end(), F(i, 1)) == graph[F(i, 2)].adjacency.end()) {
			graph[F(i, 2)].adjacency.push_back(F(i, 1));
		}
		graph[F(i, 2)].faces.push_back(i);
	}
}


std::vector<int>
getLocalMins(const std::vector<MyVertex> &graph,
	     const Eigen::MatrixXd &V,
	     std::map<int,double> &g,
	     Eigen::VectorXd &ratio,
	     Eigen::VectorXd &score,
	     double alpha, int select, double thresh)
{
	std::vector<int> res;
	for (int i = 0; i < ratio.size(); i++) {
		if (i == select) ratio[i] = 1;
		else ratio[i] = std::min(l2Dist(select, i, V) / g[i], 1.0);
		//std::cout << l2Dist(select, i, V) << " " << geodesic[select][i] << std::endl;;
		score[i] = ratio[i] + alpha * l2Dist(select, i, V);
	}

	for (int i = 0; i < score.size(); i++) {
		double currScore = score(i);
		bool minScore = true;
		for (int j = 0; j < graph[i].adjacency.size(); j++) {
			if (currScore > score[graph[i].adjacency[j]] || currScore > thresh) {
				minScore = false;
				break;
			}
		}
		if (minScore) {
			res.push_back(i);

		}

	}
	return res;
}


}
