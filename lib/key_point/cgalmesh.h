/**
 * SPDX-FileCopyrightText: Copyright © 2020 McGill University, The University of Texas at Austin
 * SPDX-FileContributor: Modified by Xinya Zhang <xinyazhang@utexas.edu>
 * SPDX-FileContributor: Robert Belfer <belfer2470@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
#ifndef LIBGEOKEY_LIB_KEY_POINT_CGALMESH_H
#define LIBGEOKEY_LIB_KEY_POINT_CGALMESH_H

#include <vector>

#include <Eigen/Core>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/Mean_curvature_flow_skeletonization.h>

#include <igl/AABB.h>

namespace geokeyconf {

typedef CGAL::Simple_cartesian<double> Kernel;
typedef Kernel::Point_3                Point;
typedef CGAL::Surface_mesh<Point>      Triangle_mesh;
typedef boost::graph_traits<Triangle_mesh>::vertex_descriptor vertex_descriptor;
typedef CGAL::Mean_curvature_flow_skeletonization<Triangle_mesh> Skeletonization;
typedef Skeletonization::Skeleton                             Skeleton;
typedef Skeleton::vertex_descriptor                           Skeleton_vertex;
typedef Skeleton::edge_descriptor                             Skeleton_edge;

inline double l2Dist(const Point& x, const Point& y)
{
	return (x - y).squared_length();
}


/*
 * Effects: (V, F) was APPENDED to sm
 */
void
igl2cgal(const Eigen::MatrixXd& V,
         const Eigen::MatrixXi& F,
         Triangle_mesh& sm);

Eigen::MatrixXd
getSurfacePoints(const Eigen::MatrixXd& intP,
                 const Triangle_mesh& tmesh,
                 const Eigen::MatrixXd& V,
                 const Eigen::MatrixXi& F);

std::vector<int>
getLocalMins(const Skeleton& skeleton, const Eigen::MatrixXd& intP, const Eigen::MatrixXd& surfP);

std::vector<int>
getMinGroups(const Skeleton& skeleton, const Eigen::MatrixXd& intP, const Eigen::MatrixXd& surfP,
             double local_tolerance_epsilon,
             double group_tolerance_epsilon);

std::vector<std::vector<int>>
getAdjacency(const Skeleton& skeleton);

Eigen::MatrixXd
adjustPoints(const igl::AABB<Eigen::MatrixXd, 3>& tree,
             const Eigen::MatrixXd& intP,
             const Eigen::MatrixXd& surfP,
             const Eigen::MatrixXd& V,
             const Eigen::MatrixXi& F);

}

#endif
