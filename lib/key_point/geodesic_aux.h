/**
 * SPDX-FileCopyrightText: Copyright © 2020 McGill University, The University of Texas at Austin
 * SPDX-FileContributor: Modified by Xinya Zhang <xinyazhang@utexas.edu>
 * SPDX-FileContributor: Robert Belfer <belfer2470@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
#ifndef LIBGEOKEY_LIB_KEY_POINT_GEODESIC_AUX_H
#define LIBGEOKEY_LIB_KEY_POINT_GEODESIC_AUX_H

#include <vector>
#include <map>

#include <Eigen/Core>
#include "alglib/optimization.h"

namespace geokeyconf {

struct MyVertex {
	double x, y, z;
	std::vector<int> adjacency;
	std::vector<int> faces; //index to faces
};
enum Valuation { Heuristic = 0, Geodesic, Euclidean };

struct VectorComp {
	bool operator() (const Eigen::Vector3d &lhs, const Eigen::Vector3d &rhs) const;
};

inline double l2Dist(int v0, int v1, const Eigen::MatrixXd& V) {
	return (V.row(v0) - V.row(v1)).norm();
}

int maxInd(const Eigen::VectorXf& vec);
int minInd(const Eigen::VectorXd& vec);
int minInd(const Eigen::VectorXf& vec);
int minInd(const Eigen::Vector3d& vec);

Eigen::Vector3d Project(const Eigen::Vector3d &q,
			const Eigen::Vector3d &A,
			const Eigen::Vector3d &B,
			const Eigen::Vector3d &C);

Eigen::Vector3d nearest(const Eigen::Vector3d &q,
			const Eigen::Vector3d &A,
			const Eigen::Vector3d &B,
			const Eigen::Vector3d &C);

void function1_grad(const alglib::real_1d_array &x,
                    double &func,
                    alglib::real_1d_array &grad,
                    void *ptr);

void generateGraph(std::vector<MyVertex> &graph,
                   const Eigen::MatrixXd &V,
                   const Eigen::MatrixXi &F);

std::vector<int>
getLocalMins(const std::vector<MyVertex> &graph,
	     const Eigen::MatrixXd &V,
	     std::map<int,double> &g,
	     Eigen::VectorXd &ratio,
	     Eigen::VectorXd &score,
	     double alpha, int select, double thresh);
}

#endif
