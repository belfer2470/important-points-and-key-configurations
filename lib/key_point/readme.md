Required library: libigl

main file: ImportantPoints.cpp

Input: 2 Arguments should be input: the .obj file, and the number of iterations that you should attempt to look for important pairs.
Default number of iterations is 12. 

Output: Outputs important points as a text file XPoints.txt in the same directory as the X.obj file.