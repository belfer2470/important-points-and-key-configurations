/**
 * SPDX-FileCopyrightText: Copyright © 2020 McGill University, The University of Texas at Austin
 * SPDX-FileContributor: Modified by Xinya Zhang <xinyazhang@utexas.edu>
 * SPDX-FileContributor: Robert Belfer <belfer2470@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
#ifdef _MSC_VER
#include <iso646.h>
#endif

#include <random>
#include <string>
#include <igl/unproject_onto_mesh.h>
#include <igl/opengl/glfw/Viewer.h>
#include <igl/opengl/glfw/imgui/ImGuiMenu.h>
#include <igl/opengl/glfw/imgui/ImGuiHelpers.h>
#include <imgui/imgui.h>
#include <igl/winding_number.h>

#include "libgeokey/KeyPoint.h"

using Viewer = igl::opengl::glfw::Viewer;
using ImGuiMenu = igl::opengl::glfw::imgui::ImGuiMenu;
using KeyPointProber = geokeyconf::KeyPointProber;
using hmap = geokeyconf::hmap;

enum Valuation { Heuristic = 0, Geodesic = 1, Euclidean = 2 };

int maxInd(const Eigen::VectorXf& vec)
{
	float best = 0;
	int res = 0;
	for (int j = 0; j < vec.size(); j++) {
		if (vec[j] > best) {
			best = vec[j];
			res = j;
		}
	}
	return res;
}

int minInd(const Eigen::VectorXd& vec)
{
	double best = 1;
	int res = 0;
	for (int j = 0; j < vec.size(); j++) {
		if (vec(j) < best) {
			best = vec(j);
			res = j;
		}
	}
	return res;
}

int minInd(const Eigen::VectorXf& vec)
{
	float best = 1;
	int res = 0;
	for (int j = 0; j < vec.size(); j++) {
		if (vec(j) < best) {
			best = vec(j);
			res = j;
		}
	}
	return res;
}

int minInd(const Eigen::Vector3d& vec)
{
	float best = 2;
	int res = 0;
	for (int j = 0; j < 3; j++) {
		if (vec(j) < best) {
			best = vec(j);
			res = j;
		}
	}
	return res;
}

Eigen::MatrixXd
color(const Eigen::MatrixXd& V,
      Valuation dir,
      const Eigen::VectorXd& score,
      hmap geodesic, // Intentionally, operator[] may alter std::map.
      int select,
      Eigen::VectorXd& geo)
{
	Eigen::MatrixXd C;
	if (dir == Heuristic) {
		igl::jet(score, true, C);
	} else if (dir == Geodesic) {
		for (int i = 0; i < score.size(); i++) {
			geo(i) = geodesic[select][i];
		}
		igl::jet(geo, true, C);
	} else if (dir == Euclidean) {
		Eigen::VectorXd eu(geodesic.size());
		for (int i = 0; i < geodesic.size(); i++) {
			eu(i) = (V.row(i) - V.row(select)).norm();
		}
		igl::jet(eu, true, C);
	} else {
		// TODO: raise exceptions
	}
	return C;
}

void draw(Viewer& viewer,
	  const std::vector<Eigen::VectorXd>& points,
	  const Eigen::MatrixXd& V,
	  const Eigen::MatrixXi& F,
	  const std::string& fout, bool write)
{
	// Note: Move the construction out of the loop
	Eigen::MatrixXd P1(1, 3);
	Eigen::MatrixXd P2(1, 3);
	Eigen::MatrixXd Pmid(1, 3);

	// TODO: should use block(...), but I'm too lazy to
	for (const auto& pair : points) {
		P1 << pair(0), pair(1), pair(2);
		P2 << pair(3), pair(4), pair(5);
		Pmid = (P1 + P2) * 0.5;

		Eigen::VectorXd W;
		viewer.data().add_points(Pmid, Eigen::RowVector3d(1, 0, 0));
		viewer.data().add_points(P1, Eigen::RowVector3d(0, 1, 0));
		viewer.data().add_points(P2, Eigen::RowVector3d(0, 1, 0));
		igl::winding_number(V, F, Pmid, W);
		std::cout << "Vertex 1 coords: " << P1 << std::endl;
		std::cout << "Vertex 2 coords: " << P2 << std::endl;
		std::cout << "Winding: " << W(0) << std::endl;

		if (write and not fout.empty()) {
			// TODO: Behavior changed. Now it's appending to the
			//       end of the file.
			std::ofstream myfile(fout, std::ios::app);
			myfile << P1 << " " << P2 << " " << W(0) << "\n";
		}

	}
}


class Visualizer {
private:
	KeyPointProber& kpp_;
	std::string name_;
	int attempts_;

	int select_ = 0;
	double visual_thresh_ = 0.8;
	double alpha_ = 0.05;
	double localMinThresh_ = 0.0;
	Valuation dir_ = Heuristic;
	Eigen::VectorXd geo_;
	Eigen::VectorXd ratio_;
	Eigen::VectorXd score_;

	int skeletonInd_ = 0;

public:
	Visualizer(KeyPointProber& kpp, const std::string& prefix, int attempts)
		:kpp_(kpp), name_(prefix), attempts_(attempts)
	{
		geo_.resize(kpp_.getV().rows());
		ratio_.resize(kpp_.getV().rows());
		score_.resize(kpp_.getV().rows());
	}

	bool callback_mouse_down(Viewer& viewer, int mouse_key, int modifier)
	{
		const auto& V = kpp_.getV();
		const auto& F = kpp_.getF();
		const auto& vcore = viewer.core(0);

		kpp_.setAlpha(alpha_);



		if (modifier == GLFW_MOD_CONTROL) {
			Eigen::Vector3f bc;
			int fid;
			// Cast a ray in the view direction starting from the mouse position
			float x = viewer.current_mouse_x;
			float y = vcore.viewport(3) - viewer.current_mouse_y;
			if (igl::unproject_onto_mesh(Eigen::Vector2f(x, y), vcore.view,
			                             vcore.proj, vcore.viewport, V, F, fid, bc))
			{
				viewer.data().clear();
				select_ = F(fid, maxInd(bc));
				

				Eigen::MatrixXd P2, P3;

				std::tie(P2, P3, ratio_, score_) = kpp_.getLocalMinFromFace(select_, visual_thresh_);

				for (int i = 0; i < P2.rows(); i++) {
					viewer.data().add_points(P2.row(i), Eigen::RowVector3d(0, 0, 1));
					viewer.data().add_points(P3.row(i), Eigen::RowVector3d(0.5, 0, 0.5));
				}

				// Note: the parameters were reordered
				//       according to
				//       https://google.github.io/styleguide/cppguide.html#Output_Parameters
				auto C = color(V, dir_, score_, kpp_.getCurrentHeatMap(), select_, geo_);

				Eigen::MatrixXd P1(1, 3);
				P1 << V.row(select_);

				// FIXME: I think this should be sufficient.
				std::cout << P1 << std::endl;
				// std::cout << V(select_, 0) << " " << V(select_, 1) << " " << V(select_, 2) << " " << std::endl;

				// FIXME: We do not really need this because V and F are constant, aren't they?
				viewer.data().set_mesh(V, F);
				viewer.data().set_colors(C);

				viewer.data().add_points(P1, Eigen::RowVector3d(1, 0, 0));

				return true;
			}
		}

		if (modifier == GLFW_MOD_SHIFT) {
			int fid2;
			Eigen::Vector3f bc;
			// Cast a ray in the view direction starting from the mouse position
			double x = viewer.current_mouse_x;
			double y = vcore.viewport(3) - viewer.current_mouse_y;
			if (igl::unproject_onto_mesh(Eigen::Vector2f(x, y), vcore.view,
			                             vcore.proj, vcore.viewport, V, F, fid2, bc))
			{
				int ind = F(fid2, maxInd(bc));
				double show;
				if (dir_ == Heuristic) {
					double res = ratio_[ind];
					show = roundf(res * 100) / 100;
				}

				if (dir_ == Geodesic) {
					double res = geo_[ind];
					show = roundf(res * 100) / 100;
				}

				if (dir_ == Euclidean) {
					double res = (V.row(select_) - V.row(ind)).norm();
					show = roundf(res * 100) / 100;
				}
				std::stringstream l1;
				l1 << show;
				viewer.data().add_label(V.row(ind), l1.str());

			}
			return true;

		}
		return false;
	}

	void callback_draw_viewer_menu(ImGuiMenu& menu, Viewer& viewer)
	{
		// Draw parent menu content
		menu.draw_viewer_menu();

		const auto& V = kpp_.getV();
		const auto& F = kpp_.getF();

		// Add new group
		if (ImGui::CollapsingHeader("Geodesic Parameters", ImGuiTreeNodeFlags_DefaultOpen))
		{
			std::mt19937 gen;
			gen.seed(std::random_device()());
			std::uniform_int_distribution<int> dist;
			auto rng = [&dist, &gen]() -> int {
				return dist(gen);
			};

			ImGui::Combo("Metric", (int *)(&dir_), "Ratio\0Geodesic\0Euclidean\0\0");
			ImGui::InputDouble("VisualThreshold", &visual_thresh_, 0, 0, "%.4f");
			ImGui::InputDouble("alpha", &alpha_, 0, 0, "%.4f");

			// Add a button
			if (ImGui::Button("Find Pairs", ImVec2(-1, 0))) {
				std::vector<Eigen::VectorXd> pairs = kpp_.findIteratively(rng);
				draw(viewer, pairs, V, F, "", false);
			}

			ImGui::InputInt("attempts", &attempts_);

			if (ImGui::Button("Find Many Pairs", ImVec2(-1, 0)))
			{
				auto points = kpp_.probeKeyPoints(attempts_);
				std::ofstream myfile(name_ + "Points.txt");
				myfile << points;
				myfile.close();

				int npts = points.rows();
				viewer.data().add_points(points.block(0, 0, npts, 3), Eigen::RowVector3d(0, 1, 0));
				viewer.data().add_points(points.block(0, 3, npts, 3), Eigen::RowVector3d(0, 1, 0));
				viewer.data().add_points((points.block(0, 0, npts, 3) + points.block(0, 3, npts, 3)) * 0.5,
							 Eigen::RowVector3d(1, 0, 0));
				myfile.close();

			}

			if (ImGui::Button("Reset", ImVec2(-1, 0)))
			{
				viewer.data().clear();
				viewer.data().set_mesh(V, F);
				viewer.data().set_face_based(true);
				kpp_.reset();
			}

		}

		if (ImGui::CollapsingHeader("Medial Skeleton View", ImGuiTreeNodeFlags_DefaultOpen))
		{
			if (ImGui::InputInt("Skeleton Index", &skeletonInd_)) {
				skeletonInd_ = std::max(0, std::min(kpp_.getSkeletonSize() -1, skeletonInd_));
			}

			if (ImGui::Button("Select", ImVec2(-1, 0)))
			{
				viewer.data().clear();
				viewer.data().set_mesh(V, F);
				viewer.data().set_face_based(true);

				Eigen::MatrixXd P1(1, 3);
				Eigen::Vector3d skelPoint;
				skelPoint = kpp_.getSkeletonPoint(skeletonInd_);
				P1 << skelPoint(0), skelPoint(1), skelPoint(2);	//direct equality is buggy for some reason
				viewer.data().add_points(P1, Eigen::RowVector3d(1, 0, 0));

				Eigen::MatrixXd P2 = kpp_.getSkeletonVertexSet(skeletonInd_);
				viewer.data().add_points(P2, Eigen::RowVector3d(0, 1, 0));
			}
			if (ImGui::InputDouble("Min Thresh ", &localMinThresh_, 0, 0, "%.4f")) {
				kpp_.setLocalMinThresh(localMinThresh_);
			}

			if (ImGui::Button("Find Notches", ImVec2(-1, 0))) {
				viewer.data().clear();
				viewer.data().set_mesh(V, F);
				viewer.data().set_face_based(true);

				auto notches = kpp_.probeNotchPoints();
				std::ofstream myfile(name_ + "Points.txt");
				myfile << notches;
				myfile.close();
				
				int npts = notches.rows();

				// Note: P3 comes before P2, check probeNotchPoints for details.
				Eigen::MatrixXd P3 = notches.block(0, 0, npts, 3);
				Eigen::MatrixXd P2 = notches.block(0, 3, npts, 3);
				Eigen::MatrixXd P1 = (P3 + P2) * .5;

				viewer.data().add_points(P1, Eigen::RowVector3d(1, 0, 0));
				viewer.data().add_points(P2, Eigen::RowVector3d(0, 1, 0));
				viewer.data().add_points(P3, Eigen::RowVector3d(0, 1, 0));
				viewer.data().add_edges(P1, P2, Eigen::RowVector3d(1, 1, 1));
				viewer.data().add_edges(P1, P3, Eigen::RowVector3d(1, 1, 1));

				std::cout << notches.rows() << std::endl;
			}

			if (ImGui::Button("Show Skeleton", ImVec2(-1, 0))) {
				viewer.data().clear();
				viewer.data().set_mesh(V, F);
				viewer.data().set_face_based(true);
				Eigen::MatrixXd P1(kpp_.getSkeletonSize(), 3);
				for (int i = 0; i < P1.rows(); i++) {
					P1.row(i) = kpp_.getSkeletonPoint(i);
				}
				viewer.data().add_points(P1, Eigen::RowVector3d(1, 0, 0));
				auto E = kpp_.getSkeletonEdges();
				for (int i = 0; i < E.rows(); i++) {
					int s = E(i,0);
					int t = E(i,0);
					viewer.data().add_edges(P1.row(s), P1.row(t), Eigen::RowVector3d(1,1,1));
				}
			}
		}
	}
};

int main(int argc, char *argv[])
{
	bool headless = false;

	std::string Inp = argc > 1 ? argv[1] : "C:\\Users\\rbelfe2\\Documents\\doublemPuzzleEnvGeodesic.obj";
	int attempts = argc > 2 ? atoi(argv[2]) : 12;
	std::string name;

	size_t pos = 0;
	if ((pos = Inp.find(".obj")) != std::string::npos) {
		name = Inp.substr(0, pos);
	}

	KeyPointProber kpp(Inp);
#if 0
	igl::HeatGeodesicsData<double> data;

	data.use_intrinsic_delaunay = true;
	double t = std::pow(igl::avg_edge_length(V, F), 2);
	igl::heat_geodesics_precompute(V, F, data);
	std::cout << "Prefactored." << std::endl;
#endif

	Viewer viewer;
	ImGuiMenu menu;
	viewer.plugins.push_back(&menu);

	double alpha = 0.05;
	double thresh = 0.8;

	if (!headless) {
		Visualizer vis(kpp, name, attempts);
		viewer.callback_mouse_down = [&vis] (Viewer& viewer, int mouse_key, int modifier) -> bool {
			return vis.callback_mouse_down(viewer, mouse_key, modifier);
		};

		menu.callback_draw_viewer_menu = [&vis, &menu, &viewer]() {
			vis.callback_draw_viewer_menu(menu, viewer);
		};

		viewer.data().set_mesh(kpp.getV(), kpp.getF());
		viewer.data().compute_normals();
		viewer.launch();
	} else {
		std::ofstream myfile(name + "Points.txt");
		myfile << kpp.probeKeyPoints(attempts);
		myfile.close();
	}

	return 0;
}
