/**
 * SPDX-FileCopyrightText: Copyright © 2020 McGill University, The University of Texas at Austin
 * SPDX-FileContributor: Modified by Xinya Zhang <xinyazhang@utexas.edu>
 * SPDX-FileContributor: Robert Belfer <belfer2470@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
#ifndef KEY_POINT_H
#define KEY_POINT_H

#include <Eigen/Core>
#include <string>
#include <tuple>
#include <memory>
#include <vector>
#include <map>
#include <functional>

namespace geokeyconf {

// FIXME: consider alternative data structure
typedef std::map<int, std::map<int, double>> hmap;
class PointRecorder;

class KeyPointProber {
public:
	KeyPointProber(const std::string& model);
	~KeyPointProber();

	// Input:
	//   attempts: number of attempts to probe
	//             Note: the returned data may not be the same as attempts
	//   seed    : PRNG seed for std::mt19937, 0 means using std::random_device()
	Eigen::MatrixXd
	probeKeyPoints(int attempts, int seed = 0, std::shared_ptr<PointRecorder> recorder = nullptr);

	// Input:
	//   seed    : PRNG seed for srand, 0 means system default
	Eigen::MatrixXd
	probeNotchPoints(unsigned long seed = 0, bool keepIntermediateData = false);

	Eigen::MatrixXd
	getIntermediateData(int iteration, int key) const;

	double getAlpha() const { return alpha_; }
	void setAlpha(double alpha) { alpha_ = alpha; }

	double getLocalMinThresh() const { return localMinThresh_; }
	void setLocalMinThresh(double new_thresh) { localMinThresh_ = new_thresh; }

	double getToleranceEpsilon() const { return tolerance_epsilon_; }
	void setToleranceEpsilon(double epsilon) { tolerance_epsilon_ = epsilon; }

	// Returns
	//  P2, P3, score
	std::tuple<Eigen::MatrixXd, Eigen::MatrixXd, Eigen::VectorXd, Eigen::VectorXd>
	getLocalMinFromFace(int select, double thresh) const;

	std::vector<Eigen::VectorXd> findIteratively(std::function<int()> rng,
                                                 std::shared_ptr<PointRecorder> recorder);

	const Eigen::MatrixXd& getV() const;
	const Eigen::MatrixXi& getF() const;
	const hmap& getCurrentHeatMap() const; // Note: this may be updated during queries

	int getSkeletonSize() const;
	Eigen::Vector3d getSkeletonPoint(int index) const;
	Eigen::MatrixXd getAllSkeletonPoints() const;

	Eigen::MatrixXd
	getSkeletonVertexSet(int index) const;

	Eigen::MatrixXi getSkeletonEdges() const;

	void reset();

private:
	double alpha_ = 0.05;
	double localMinThresh_ = 0.10;
	double tolerance_epsilon_ = 0.05;

	struct InternalData;
	std::shared_ptr<InternalData> d_;
};

}

#endif
