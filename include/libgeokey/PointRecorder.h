/**
 * SPDX-FileCopyrightText: Copyright © 2020 McGill University, The University of Texas at Austin
 * SPDX-FileContributor: Modified by Xinya Zhang <xinyazhang@utexas.edu>
 * SPDX-FileContributor: Robert Belfer <belfer2470@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
#ifndef POINT_RECORDER_H
#define POINT_RECORDER_H

#include <Eigen/Core>
#include <vector>

namespace geokeyconf {

class PointRecorder {
public:
    PointRecorder();
    ~PointRecorder();

    void record_pair(const Eigen::Vector3d& v, const Eigen::Vector3d& w,
                     double geometric_distance, double score);
    
    Eigen::MatrixXd report_pairs() const;
private:
    std::vector<std::tuple<Eigen::Vector3d, Eigen::Vector3d, double, double>> pairs_;
};

}

#endif
