/**
 * SPDX-FileCopyrightText: Copyright © 2020 McGill University, The University of Texas at Austin
 * SPDX-FileContributor: Modified by Xinya Zhang <xinyazhang@utexas.edu>
 * SPDX-FileContributor: Robert Belfer <belfer2470@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
#pragma once

#ifndef KEY_SAMPLER_H
#define KEY_SAMPLER_H

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <tuple>
#include <memory>

class PQP_Model;

namespace geokeyconf {

class KeySampler {
public:
	KeySampler(const std::string& env_fn, const std::string& rob_fn);
	~KeySampler();

	bool isMyStateValid(Eigen::Quaterniond q, Eigen::Vector3d t);
	bool isMyStateValid(Eigen::Quaterniond q, Eigen::Vector3d t,
	                    double &distance);
	bool isMyStateValid(Eigen::Matrix3d R, Eigen::Vector3d robPV,
	                    Eigen::Vector3d envPV);
	bool isMyStateValid(Eigen::Matrix3d R, Eigen::Vector3d robPV,
	                    Eigen::Vector3d envPV, double &distance);
	bool isMyStateValid(double angle1, double angle2, Eigen::Vector3d axis1,
	                    Eigen::Vector3d axis2, Eigen::Matrix3d Ortho,
	                    Eigen::Vector3d robPV, Eigen::Vector3d envPV);
	bool isMyStateValid(double angle1, double angle2, Eigen::Vector3d axis1,
	                    Eigen::Vector3d axis2, Eigen::Matrix3d Ortho,
	                    Eigen::Vector3d robPV, Eigen::Vector3d envPV,
	                    double &distance);
	bool isMyStateValid(double angle1, Eigen::Vector3d axis1,
	                    Eigen::Matrix3d Onto, Eigen::Vector3d robPV,
	                    Eigen::Vector3d envPV);
	bool isMyStateValid(double angle1, Eigen::Vector3d axis1,
	                    Eigen::Matrix3d Onto, Eigen::Vector3d robPV,
	                    Eigen::Vector3d envPV, double &distance);

	// Returns:
	//  0: key configurations
	//  1: indices of key point from ENV
	//  2: indices of key point from ROB
	std::tuple<Eigen::MatrixXd, Eigen::VectorXi, Eigen::VectorXi>
	getAllKeyConfigs(Eigen::MatrixXd env_keypoints,
			 Eigen::MatrixXd rob_keypoints,
			 int number_of_rotations);

	Eigen::MatrixXd
	getKeyConfigs(Eigen::VectorXd env_keyp,
		      Eigen::VectorXd rob_keyp,
		      int div1,
		      int div2 = 1);

	std::tuple<Eigen::MatrixXd, Eigen::VectorXi>
	getKeyConfigsInOut(Eigen::VectorXd env_keyp,
			   Eigen::VectorXd rob_keyp,
			   int div1, int div2,
			   int rotationMode);

	std::tuple<Eigen::MatrixXd, Eigen::MatrixXi>
	getKeyConfigsBothOutside(Eigen::VectorXd env_keyp,
				 Eigen::VectorXd rob_keyp,
				 int div1, int div2);

	double getThreshNarrowTunnelRatio() const { return thresh_narrow_tunnel_ratio_; }
	void setThreshNarrowTunnelRatio(double nt) { thresh_narrow_tunnel_ratio_ = nt; }
    
    void setDisableStateValidation(bool disable) { disableStateValidation_ = disable; }
private:
	std::shared_ptr<PQP_Model> pqpEnv_;
	std::shared_ptr<PQP_Model> pqpRob_;
	// Adapter for legacy code
	PQP_Model *pqpEnv, *pqpRobot;
	double thresh_narrow_tunnel_ratio_ = 2.0;
    bool disableStateValidation_ = false;
};

}

#endif
